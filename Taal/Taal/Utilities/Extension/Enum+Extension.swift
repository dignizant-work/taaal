//
//  Enum+Extension.swift
//  Tailory
//
//  Created by Jaydeep on 28/08/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import Foundation
import UIKit

enum enumForLoginRegisterOTP {
    case update
    case login
    case register
}

enum enumAddressTitle {
    case addAddress
    case editAddress
}

enum enumAddressVcCheck {
    case regular
    case addCase
    case acceptedQuotation
    case isEditCase
}

enum enumAddCaseVcCheck {
    case addCase
    case editCase
}

enum enumCheckChatDetailVC {
    case regular
    case requestDetailVC
}

enum enumHistoryParent {
    case fromTab
    case fromProfile
}
