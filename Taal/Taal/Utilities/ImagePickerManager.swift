//
//  ImagePickerManager.swift
//  Taal
//
//  Created by Haresh on 06/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit
import Photos

class ImagePickerManager: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var picker = UIImagePickerController()
    var alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
    var viewController: UIViewController?
    var pickImageCallback : ((UIImage,String,Data?) -> ())?
    
    override init(){
        super.init()
    }
    
    func pickImage(_ viewController: UIViewController, _ callback: @escaping ((UIImage,String,Data?) -> ())) {
        pickImageCallback = callback;
        self.viewController = viewController;
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default){
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallary", style: .default){
            UIAlertAction in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel){
            UIAlertAction in
        }
        
        // Add the actions
        picker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        alert.popoverPresentationController?.sourceView = self.viewController!.view
        viewController.present(alert, animated: true, completion: nil)
    }
    func openCamera(){
        alert.dismiss(animated: true, completion: nil)
        if(UIImagePickerController .isSourceTypeAvailable(.camera)){
            picker.sourceType = .camera
            self.viewController!.present(picker, animated: true, completion: nil)
            
            
        } else {
            Utility.shared.showAlertAtBottom(strMessage: "You don't have camera")
            //            let alertWarning = UIAlertView(title:"Warning", message: "You don't have camera", delegate:nil, cancelButtonTitle:"OK", otherButtonTitles:"")
            //            alertWarning.show()
        }
    }
    func openGallery(){
        alert.dismiss(animated: true, completion: nil)
        picker.sourceType = .photoLibrary
        self.viewController!.present(picker, animated: true, completion: nil)
        
    }
    
    func convertToGIF(imageURL:URL, completion:@escaping(UIImage?, Data?) -> Void) {
        guard let asset = PHAsset.fetchAssets(withALAssetURLs: [imageURL], options: nil).lastObject else {
            completion(nil, nil)
            return
        }
        var imgGIF:UIImage?
        var dataGIF:Data?
        if picker.sourceType == .photoLibrary || picker.sourceType == .savedPhotosAlbum {
            let options = PHImageRequestOptions()
            options.isSynchronous = false
            options.isNetworkAccessAllowed = true
            options.resizeMode = .exact
            options.deliveryMode = .highQualityFormat
            options.version = .original
            PHImageManager.default().requestImageData(for: asset, options: options) { data, uti, orientation, info in
                guard let info = info else {
                    completion(imgGIF,dataGIF)
                    return
                }
                
                if let error = info[PHImageErrorKey] as? Error {
                    print("Cannot fetch data for GIF image: \(error)")
                    completion(imgGIF,dataGIF)
                    return
                }
                
                if let isInCould = info[PHImageResultIsInCloudKey] as? Bool, isInCould {
                    print("Cannot fetch data from cloud. Option for network access not set.")
                    completion(imgGIF,dataGIF)
                    return
                }
                
                // do something with data (it is a Data object)
                if let imageData = data {
                    dataGIF = imageData
                    imgGIF = UIImage.animatedImage(data: imageData)
                }
                completion(imgGIF,dataGIF)
                
            }
        } else {
            // do something with media taken via camera
            completion(imgGIF,dataGIF)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: false, completion: nil)
        
        
        let imgTypeInfo = info[picker.allowsEditing ? .editedImage : .originalImage]
        
        var fileName = "img_\(Date().timeIntervalSince1970)"
        
        if let assetPath = info[UIImagePickerController.InfoKey.referenceURL] as? URL {
            print("assetPath.absoluteString:=",assetPath.absoluteString)
            if (assetPath.absoluteString.lowercased().contains("gif")) {
                fileName.append(".gif")
                print("GIF")
                convertToGIF(imageURL: assetPath) { [weak self] (gifImage,gifData)  in
                    if let imageGIF = gifImage {
                        self?.pickImageCallback?(imageGIF, fileName, gifData)
                    }
                }
                return
            }
            var imgData:Data?
            
            guard var image = imgTypeInfo as? UIImage else {
                fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
            }
            if (assetPath.absoluteString.lowercased().contains("jpg")) {
                fileName.append(".jpg")
                if let jpgData = image.jpegData(compressionQuality: 0.5) {
                    imgData = jpgData
                }
                print("JPG")
            } else if (assetPath.absoluteString.lowercased().contains("png")) {
                fileName.append(".png")
                if let pngData = image.pngData() {
                    imgData = pngData
                }
                print("PNG")
            } else {
                fileName.append(".jpg")
                print("Unknown")
                if let jpgData = image.jpegData(compressionQuality: 0.5), let jpg = UIImage.init(data: jpgData) {
                    image = jpg
                    imgData = jpgData
                }
            }
            
            pickImageCallback?(image, fileName, imgData)
            
        }
        
        
        
    }
    
    
    @objc func imagePickerController(_ picker: UIImagePickerController, pickedImage: UIImage?) {
        
    }
    
}



extension UIImage {
    
    static func animatedImage(data: Data) -> UIImage? {
        guard let source: CGImageSource = CGImageSourceCreateWithData(data as CFData, nil), CGImageSourceGetCount(source) > 1 else {
            return UIImage(data: data)
        }
        
        // Collect key frames and durations
        var frames: [(image: CGImage, delay: TimeInterval)] = []
        for i: Int in 0 ..< CGImageSourceGetCount(source) {
            guard let image = CGImageSourceCreateImageAtIndex(source, i, nil), let frame = CGImageSourceCopyPropertiesAtIndex(source, i, nil) as? [String: Any], let gif = frame["{GIF}"] as? [String: Any] else {
                continue
            }
            
            // Mimic WebKit approach to determine frame delay
            if let delay = gif["UnclampedDelayTime"] as? TimeInterval, delay > 0.0 {
                frames.append((image, delay)) // Prefer "unclamped" delay time
            } else if let delay = gif["DelayTime"] as? TimeInterval, delay > 0.0 {
                frames.append((image, delay))
            } else {
                frames.append((image, 0.1)) // WebKit default
            }
        }
        
        // Convert key frames to animated image
        var images: [UIImage] = []
        var duration: TimeInterval = 0.0
        for frame in frames {
            let image = UIImage(cgImage: frame.image)
            for _ in 0 ..< Int(frame.delay * 100.0) {
                images.append(image) // Add fill frames
            }
            duration += frame.delay
        }
        return UIImage.animatedImage(with: images, duration: duration)
    }
}
