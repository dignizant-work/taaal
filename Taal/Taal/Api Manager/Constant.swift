//
//  Constant.swift
//  Taal
//
//  Created by om on 11/13/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD


//MARK: - Enum
enum AppStoryboard : String {
    case Main = "Main"
    case Home = "Home"
    case Account = "Account"
    case Profile = "Profile"
    var instance : UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
}

func setTableView(_ message: String, tableView: UITableView) {
    let noDataLabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: tableView.frame.size.height))
    
    noDataLabel.textAlignment = .center
    noDataLabel.text = message
    noDataLabel.numberOfLines = 0
    noDataLabel.textColor = UIColor.appThemeBlueColor
    noDataLabel.font = themeFont(size: 18, fontname: .regular)
    noDataLabel.center = tableView.center
    tableView.backgroundView = noDataLabel
}

func isValidEmail(testStr:String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: testStr)
}

///--- Hours, minutes,  seconds
func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
  return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
}

func randomString(length: Int) -> String {
    let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    return String((0...length-1).map{ _ in letters.randomElement()! })
}

//MARK: - Functions Color
func ColorOrange() -> UIColor {
    return UIColor(red: 218/255, green: 115/255, blue: 64/255, alpha: 1)
}

func ColorGraydark() -> UIColor{
    return UIColor(red: 244/255, green: 244/255, blue: 245/255, alpha: 1)
}

func ColorGray() -> UIColor{
    return UIColor(red: 163/255, green: 165/255, blue: 168/255, alpha: 1)
}

func ColorNavigationBar() -> UIColor {
    return UIColor(red: 0/255, green: 68/255, blue: 103/255, alpha: 1)
}

func ColorNavigationStatusBar() -> UIColor {
    return UIColor(red: 13/255, green: 65/255, blue: 100/255, alpha: 1)
}


//MARK: - Extensions

extension UIViewController {
    func goBack(animated: Bool = true) {
        self.navigationController?.popViewController(animated: animated)
    }
    
    func goBackRoot(animated: Bool = true) {
        self.navigationController?.popToRootViewController(animated: animated)
    }
    
    func dismissScreen(animated: Bool = true) {
        self.dismiss(animated: animated, completion: nil)
    }
    
}

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    func jpeg(_ quality: JPEGQuality) -> Data? {
        return UIImage().jpegData(compressionQuality: quality.rawValue)
//        return UIImageJPEGRepresentation(self, quality.rawValue)
    }
    
    func png(_ quality: JPEGQuality) -> Data? {
        return self.pngData()
    }
}


extension String {
    
    public func toURL() -> URL? {
        return URL(string: self)
    }
    
    func toTrim() -> String {
        let trimmedString = self.trimmingCharacters(in: .whitespacesAndNewlines)
        return trimmedString
    }
}
