//
//  ApiConstant.swift
//  Taal
//
//  Created by om on 11/13/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

//MARK: - Main URLS
//let SERVER_URL = "http://173.231.196.229/~tvavisa/taal/api/"
//let screenURL = "https://test-api-Taal.outsourcedglobal.com/uploads/"

//-- Google PlacePicker key
let key_google_place_picker = "AIzaSyB3kBdoe2NQA4t17o8xlu_7TuSS0iZISCc"

//let key_google_place_picker =  "AIzaSyAI8Oi8hv4wq2YbAMZho9jC86wwzXlf4W0"

//-- One Signal authentication
let oneSignalAppId          = "d338869b-a472-4fec-becd-45760af3aaee"

//MARK: - URLS Names
//let mainURL = "http://173.231.196.229/~tvavisa/taal/"
//let mainURL               = "http://taal.dignizantapps.com/"
//let mainURL               = "http://3.19.240.124/taal/"     //Test url
let mainURL                 = "http://www.taaal.com/"       //Live url
//let mainURL                 = "http://52.66.128.99/taal/"       //Test url

//Base screen
let screenBase              = "api/screen/"


//User Cases
let getRequestParametersURL = "api/user/getCaseParameters/"     // {category_id} Get Request Parameters
let addCaseURL              = "api/user/addCase"                // Add case
let getCasesURL             = "api/user/getCases"               // Cases
let getcaseDetailsURL       = "api/user/getCaseDetails/"        // {case_id} Case Details
let getCaseHistoryURL       = "api/user/getCaseHistory/"        // {status_id} Cases
let getCaseHistoryDetailsURL = "api/user/getCaseHistoryDetails/" // {case_id} Case History Details
let quotationAcceptedURL    = "api/user/quotationAccepted"      // Quotation Accepted
let caseCancelledURL        = "api/user/caseCancelled/"         // {case_id} Case Cancelled
let editCaseURL             = "api/user/editCase/"              // {case_id} Edit case
let updateCaseURL           = "api/user/updateCase/"            // {case_id} Update case
let deleteCaseURL           = "api/user/deleteCase/"            //{id} Delete Case by Id


//updateSettings
let updateSettingsURL       = "api/user/updateSettings"         //updateSettings


//Change language
let changeLangURL           = "api/user/changeLang"             //changeLang


//User Chat
let sendMessageToUserURL    = "api/chat/sendMessageToProvider" //Send Message
let userChatHistoryURL      = "api/chat/userChatHistory/"      //User ChatHistory
let userChatListURL         = "api/chat/userChatList"          //User ChatList


//Pages ---- for TermsOfUse(1) and PrivacyPolicy(2)
let getPagesURL             = "api/user/getPages/"              // Get Taal's Pages


//User entry
let userRegisterURL         = "api/user/register"               //Register a company to app
let userLoginURL            = "api/user/login"                  //Login a user to the app
let userLogoutURL           = "api/user/logout"                 //Logout the user from the app
let userMobileEmailExistURL = "api/user/mobileEmailExist"       //check Mobile or Email while login


//User Address
let addAddressURL           = "api/user/addAddress"             //Add User address
let getAddressByIdURL       = "api/user/getAddressById/"        // {id} Get User address by ID
let getAddressesURL         = "api/user/getAddresses"           //Get all addresses of a user
let setDefaultAddressURL    = "api/user/setDefaultAddress/"     // {address_id} Set default address
let editAddressURL          = "api/user/editAddress"            //Edit Address
let deleteAddressByIdURL    = "api/user/deleteAddressById/"     // {address_id} Delete user address


//User's Review-Ratings
let rateProviderURL         = "api/user/rateProvider"           //User rating & review
let getRatingReviewsURL     = "api/user/getRatingReviews/"      //Get Provider Ratings-Reviews


//caseChangeStatus
let caseChangeStatusURL     = "api/user/caseChangeStatus"       //caseChangeStatus


//Category List
let categoryListURL         = "api/category"


//User Profile
let getUserProfileURL       = "api/user/getProfile"             //Get User Profile
let updateUserProfileURL    = "api/user/updateProfile"          //Update User profile
let updateUserMobileNumber  = "api/user/updateMobileNumber"     //Update User's Mobile number


//Countries
let getAllCountriesURL      = "api/user/countries"              //Get all countries


//Cities
let getAllCitiesURL         = "api/user/cities/"                //Get all cities based on the country ID


//Zones
let getAllZonesURL          = "api/user/zones/"                 //Get all zones based on the country ID


//Add feedback
let addFeedbackURL          = "api/user/feedback"               //Add feedback in app

//Logout:
let logoutURL               = "/api/user/logout"                //Logout the user from the app

//privacy-and-policy:
let termsAndPrivacyURL      = "api/termsAndPrivacy"            // termsAndPrivacy <For T&C param pass : "setting_type" = "setting_type", For privacy policy param: "setting_type" = "privacy-and-policy" >

