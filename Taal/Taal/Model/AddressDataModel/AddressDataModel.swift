//
//  AddressDataModel.swift
//  Taal
//
//  Created by Vishal on 10/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation
import ObjectMapper

class AddressDataModel: Mappable {

    var id: NSNumber?
    var name: String = ""
    var block: String = ""
    var street: String = ""
    var area: String = ""
    var city: String = ""
    var cityId: NSNumber?
    var zoneId: NSNumber?
    var building: String = ""
    var mobile: String = ""
    var floor: String = ""
    var notes: String = ""
    var countryId: NSNumber?
    var isDefault: NSNumber?
    var areaName: String = ""

    required init?(map: Map){
    }

    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        block <- map["block"]
        street <- map["street"]
        area <- map["area"]
        city <- map["city"]
        cityId <- map["city_id"]
        zoneId <- map["zone_id"]
        building <- map["building"]
        mobile <- map["mobile"]
        floor <- map["floor"]
        notes <- map["notes"]
        countryId <- map["country_id"]
        isDefault <- map["is_default"]
        areaName <- map["area_name"]
    }
}

