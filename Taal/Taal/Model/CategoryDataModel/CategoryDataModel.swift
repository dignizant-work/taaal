import Foundation 
import ObjectMapper 

class CategoryDataModel: Mappable { 

	var caseId: NSNumber? 
	var categoryName: String? 
	var categoryImage: String? 
	var status: String? 
	var hasAudio: NSNumber?
    var offeredPrice: String = ""
    var voiceNotes: String = ""
    var extraNotes:String = ""
    var quotationId: NSNumber?
    var isDelete: Bool = true
    var isEdit: Bool = true
    var price : String = ""
    var categoryId = NSNumber()
    var voiceUrl : String = ""
    var audioBase64 = String()
    var quotationExtraNotes:String = ""
    var id: NSNumber?
    var isSubCat: Bool?
    var name: String?
    var imageUrl: String?
    var type: String?
    var datatype: String?
    var value: [String]?
    var required: Bool?
    var label: String?
    var valuePassed: Bool = false
    var enterValue = ""
    var arrayImage : [UIImage] = []
    
    var countUnreadQuottion: NSNumber?
    var category: Category?
    var fields: [Fields] = []
    var numQuotations: NSNumber?
    
    var caseDetails: CaseDetails?
    var quotationDetails: [QuotationDetails]?
    var editValue: [String]?
    var providerId = NSNumber()
    var statusId = NSNumber()
    
    
	var caseDescription: [CaseDescription]?
    var userDetails: UserDescription?

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
        
		caseId <- map["case_id"] 
		categoryName <- map["category_name"] 
		categoryImage <- map["category_image"] 
		status <- map["status"] 
		hasAudio <- map["hasAudio"] 
		caseDescription <- map["case_description"]
        userDetails <- map["user_details"]
        userDetails <- map["userDetails"]
        offeredPrice <- map["offered_price"]
        voiceNotes <- map["voice_notes"]
        extraNotes <- map["extra_notes"]
        quotationId <- map["quotation_id"]
        isDelete <- map["is_delete"]
        isEdit <- map["is_edit"]
        price <- map["price"]
        categoryId <- map["category_id"]
        voiceUrl <- map["voiceUrl"]
        audioBase64 <- map["audioBase64"]
        quotationExtraNotes <- map["quotationExtraNotes"]
        id <- map["id"]
        isSubCat <- map["isSubCat"]
        name <- map["name"]
        imageUrl <- map["imageUrl"]
        type <- map["type"]
        datatype <- map["datatype"]
        value <- map["value"]
        required <- map["required"]
        label <- map["label"]
        valuePassed <- map["valuePassed"]
        enterValue <- map["enterValue"]
        arrayImage <- map["arrayImage"]
        countUnreadQuottion <- map["count_unreadQuottion"]
        fields <- map["fields"]
        category <- map["category"]
        numQuotations <- map["num_quotations"]
        caseDetails <- map["CaseDetails"]
        quotationDetails <- map["quotationDetails"]
        editValue <- map["edit_value"]
        providerId <- map["provider_id"]
        statusId <- map["status_id"]
	}
} 

class CaseDescription: Mappable { 

	var label: String? 
	var value: [String]? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		label <- map["label"] 
		value <- map["value"] 
	}
} 

class UserDescription: Mappable {
    
    var name: String?
    var address: String?
    var conversationId: String = ""
    var longitudeStr = String()
    var latitudeStr = String()
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        address <- map["address"]
        conversationId <- map["conversation_id"]
        latitudeStr <- map["latitude"]
        longitudeStr <- map["longtitude"]
    }
}

class Fields: Mappable {

    var type: String?
    var value: [String]?
    var label: String?

    required init?(map: Map){
    }

    func mapping(map: Map) {
        type <- map["type"]
        value <- map["value"]
        label <- map["label"]
    }
}

class Category: Mappable {

    var image: String?
    var name: String?

    required init?(map: Map){
    }

    func mapping(map: Map) {
        image <- map["image"]
        name <- map["name"]
    }
}

class QuotationDetails: Mappable {

    var id: NSNumber?
    var price: String?
    var provider: Provider?
    var extraNotes: String?
    var voiceNotes: String?

    required init?(map: Map){
    }

    func mapping(map: Map) {
        id <- map["id"]
        price <- map["price"]
        provider <- map["provider"]
        extraNotes <- map["extra_notes"]
        voiceNotes <- map["voice_notes"]
    }
}

class Provider: Mappable {

    var providerId: NSNumber?
    var providerName: String?
    var providerImage: Any?
    var providerRating: NSNumber?
    var providerMobile: String?
    var providerCountryCode: String?

    required init?(map: Map){
    }

    func mapping(map: Map) {
        providerId <- map["provider_id"]
        providerName <- map["provider_name"]
        providerImage <- map["provider_image"]
        providerRating <- map["provider_rating"]
        providerMobile <- map["provider_mobile"]
        providerCountryCode <- map["provider_country_code"]
    }
}

class CaseDetails: Mappable {

    var fields: [Fields]?
    var category: Category?
    var status: String?
    var caseId: NSNumber?
    var numQuotations: NSNumber?
    var isEdit: Bool?
    var isDelete: Bool?
    var statusId = NSNumber()

    required init?(map: Map){
    }

    func mapping(map: Map) {
        fields <- map["fields"]
        category <- map["category"]
        status <- map["status"]
        caseId <- map["case_id"]
        numQuotations <- map["num_quotations"]
        isEdit <- map["is_edit"]
        isDelete <- map["is_delete"]
        statusId <- map["status_id"]
    }
}

