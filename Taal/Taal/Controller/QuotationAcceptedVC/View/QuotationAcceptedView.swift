//
//  QuotationAcceptedView.swift
//  Taal
//
//  Created by Vishal on 29/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit

class QuotationAcceptedView: UIView {
    
    //MARK: Outlets
    @IBOutlet var lblgreat: UILabel!
    @IBOutlet var lblRequestCompletedSucceessfully: UILabel!
    @IBOutlet var btnContinue: UIButton!
    
    
    func setUpUI() {
        
        [lblgreat].forEach { (lbl) in
            lbl?.textColor = .white
            lbl?.font = themeFont(size: 22, fontname: .semiCn)
        }
        
        [lblRequestCompletedSucceessfully].forEach { (lbl) in
            lbl?.textColor = .white
            lbl?.font = themeFont(size: 19, fontname: .semiCn)
        }
        
        lblgreat.text = "Great_key".localized
        lblRequestCompletedSucceessfully.text = "Your_request_has_been_completed_successfully_key".localized
        
        btnContinue.setTitle("Continue_key".localized, for: .normal)
        btnContinue.setupThemeButtonUI()
        btnContinue.backgroundColor = UIColor.appThemeLightOrangeColor
    }
}
