//
//  QuotationAcceptedVC.swift
//  Taal
//
//  Created by Vishal on 29/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit

class QuotationAcceptedVC: UIViewController {

    var isCheckController = enumAddressVcCheck.acceptedQuotation
    
    lazy var mainView: QuotationAcceptedView = { [unowned self] in
    return self.view as! QuotationAcceptedView
    }()
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainView.setUpUI()
        self.setUpNavigationBarWithTitleRightAndBack(strTitle: "", barColor: UIColor.appThemeBlueColor, showTitle: false, isLeftBackButton: false, isRightSkipButton: false)
        
        if self.isCheckController == .addCase {
            self.mainView.lblRequestCompletedSucceessfully.text = "Quotation_Accepted_new_key".localized
//            self.mainView.lblRequestCompletedSucceessfully.text = "Quotation_Accepted_key".localized
        }
    }
}

//MARK: Button Action
extension QuotationAcceptedVC {
    
    @IBAction func btnCountinueAction(_ sender: Any) {
              
        if self.isCheckController == .addCase {
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
            for aViewController in viewControllers {
                if aViewController is HomeVC {
                    self.navigationController!.popToViewController(aViewController, animated: true)
                }
            }
        }
        else if self.isCheckController == .acceptedQuotation || self.isCheckController == .isEditCase {
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
            for aViewController in viewControllers {
                if aViewController is MyRequestVC {
                    handlorAcceptedQuotation()
                    self.navigationController!.popToViewController(aViewController, animated: true)
                }
            }
        }
    }
}
