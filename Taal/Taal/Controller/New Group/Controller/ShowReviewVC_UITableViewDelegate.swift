//
//  ShowReviewVC_UITableViewDelegate.swift
//  Taal
//
//  Created by Vishal on 30/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit

extension ShowReviewVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.mainModelView.arrayReviewData.count == 0 {
            
            setTableView("No_record_found_key".localized, tableView: self.mainView.tblReview)
            return 0
        }
        
        tableView.backgroundView = nil
        return self.mainModelView.arrayReviewData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShowReviewTableCell", for: indexPath) as! ShowReviewTableCell
        
        let dict = self.mainModelView.arrayReviewData[indexPath.row]
        cell.setUpData(data: dict)
        
        return cell
    }
    
    // Set observer for inner table height
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if(keyPath == "contentSize"){
            if object is UITableView {
                
                 print("contentSize:= \(self.mainView.tblReview.contentSize.height)")
                self.view.layoutIfNeeded()
                self.view.layoutSubviews()
                
                let tblHeight = self.mainView.tblReview.contentSize.height
                let constant = UIScreen.main.bounds.size.height-150
                
                if self.mainModelView.arrayReviewData.count > 0 {
                    if tblHeight > constant {
                        self.mainView.tblReviewHeightonstraint.constant = constant
                        self.mainView.tblReview.isScrollEnabled = true
                    }
                    else {
                        self.mainView.tblReviewHeightonstraint.constant = tblHeight
                        self.mainView.tblReview.isScrollEnabled = false
                    }
                }
                else {
                    self.mainView.tblReviewHeightonstraint.constant = 75
                }
            }
        }
    }
}
