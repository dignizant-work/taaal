//
//  InvoiceView.swift
//  Taal
//
//  Created by Vishal on 29/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit

class InvoiceView: UIView {
    
    //MARK: Outlets
    @IBOutlet var vwMain: UIView!
    @IBOutlet var lblRequest: UILabel!
    @IBOutlet weak var lblRequestValue: UILabel!
    
    @IBOutlet var lblCategoryTitle: UILabel!
    @IBOutlet var lblCategoryValue: UILabel!
    
    @IBOutlet var lblDescriptionTitle: UILabel!
    @IBOutlet var tblDescription: UITableView!
    @IBOutlet weak var tblDescriptionHeightConstrain: NSLayoutConstraint!
    
    @IBOutlet var lblProviderTitle: UILabel!
    @IBOutlet var lblProviderValue: UILabel!
    
    @IBOutlet var lblQuotationDetails: UILabel!
    @IBOutlet var lblQuotationDetailTitle: UILabel!
    @IBOutlet var lblQuotationDetailValue: UILabel!
    
    @IBOutlet var lblOffredPriceTitle: UILabel!
    @IBOutlet var lblOffredPriceValue: UILabel!
    
    @IBOutlet var lblUserDetailsTitle: UILabel!
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblAddress: UILabel!
    
    @IBOutlet weak var bottomView: CustomView!
    
    @IBOutlet weak var btnCall: CustomButton!
    @IBOutlet weak var btnChat: CustomButton!
    @IBOutlet weak var btnTrackNow: CustomButton!
    @IBOutlet weak var btnCompletedJob: CustomButton!
    
    @IBOutlet var bntConfirm: CustomButton!
    @IBOutlet weak var vwImages: UIView!
    @IBOutlet weak var collectionImages: UICollectionView!
    @IBOutlet weak var pageController: UIPageControl!
    
    @IBOutlet weak var vwAudio: UIView!
    @IBOutlet weak var lblAudioTimer: UILabel!
    @IBOutlet weak var indicaterAudio: UISlider!
    @IBOutlet weak var btnAudioOnOff: UIButton!
    
    @IBOutlet weak var vwAcceptReject: UIView!
    @IBOutlet weak var btnAccept: CustomButton!
    @IBOutlet weak var btnReject: CustomButton!
    
    
    
    //MARK: SetUp
    func setUpUI(theController: InvoiceVC) {
        
        [lblRequest, lblRequestValue].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlueColor
            lbl?.font = themeFont(size: 18, fontname: .semiCn)
        }
        
        [lblCategoryTitle, lblDescriptionTitle, lblProviderTitle, lblQuotationDetailTitle, lblOffredPriceValue ].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlueColor
            lbl?.font = themeFont(size: 18, fontname: .boldSemi)
        }
        
        [lblCategoryValue, lblProviderValue].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlueColor
            lbl?.font = themeFont(size: 12, fontname: .regular)
        }
        
        [lblQuotationDetails, lblOffredPriceTitle].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeOrangeColor
            lbl?.font = themeFont(size: 12, fontname: .regular)
        }
        
        [lblQuotationDetailValue].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlueColor
            lbl?.font = themeFont(size: 12, fontname: .light)
        }
        
        [lblAudioTimer].forEach { (lbl) in
            lbl?.textColor = UIColor.black
            lbl?.font = themeFont(size: 12, fontname: .light)
        }
        
        [lblUserDetailsTitle,lblName, lblAddress, lblQuotationDetailValue].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlueColor
            lbl?.font = themeFont(size: 12, fontname: .regular)
        }
                
        [bntConfirm, btnAccept, btnReject].forEach { (btn) in
            btn?.setupThemeButtonUI()
            btn?.backgroundColor = UIColor.appThemeLightOrangeColor
            btn?.titleLabel?.font = themeFont(size: 18, fontname: .regular)
        }
        [btnCall,btnChat,btnTrackNow, btnCompletedJob].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 18, fontname: .bold)
        }
        
        btnCall.setImage(UIImage(named: "ic_call")?.imageFlippedForRightToLeftLayoutDirection(), for: .normal)
        btnCall.setTitle("Call_key".localized, for: .normal)
        btnChat.setImage(UIImage(named: "ic_chat")?.imageFlippedForRightToLeftLayoutDirection(), for: .normal)
        btnChat.setTitle("Chat_key".localized, for: .normal)
        
        self.btnTrackNow.isHidden = true
        self.btnCompletedJob.isHidden = true
        
        self.tblDescription.register(UINib(nibName: "DescriptionTableCell", bundle: nil), forCellReuseIdentifier: "DescriptionTableCell")
        self.collectionImages.register(UINib(nibName: "InvoiceCollectionCell", bundle: nil), forCellWithReuseIdentifier: "InvoiceCollectionCell")
        vwImages.isHidden = true
        
        lblRequest.text = "Request_key".localized
        lblCategoryTitle.text = "Category_key".localized
        lblDescriptionTitle.text = "Description_key".localized
        lblProviderTitle.text = "Provider_key".localized
        lblQuotationDetails.text = "Quotation_details_key".localized
        lblOffredPriceTitle.text = "Offred_Price_key".localized
        bntConfirm.setTitle("Confirm_key".localized, for: .normal)
        lblUserDetailsTitle.text = "User_detail_key".localized
        btnTrackNow.setTitle("Track_now_key".localized, for: .normal)
        btnCompletedJob.setTitle("Confirm_key".localized, for: .normal)
        btnAccept.setTitle("Accept".localized, for: .normal)
        btnReject.setTitle("Reject".localized, for: .normal)
        
        self.vwAcceptReject.isHidden = true
    }
    
    func setUpAcceptedQuotationData(categoryData: CategoryDataModel, quotationData: QuotationDetails, addressData: AddressDataModel) {
        
        self.tblDescription.reloadData()
        
        self.lblRequestValue.text = "\(categoryData.caseDetails?.caseId ?? 0)"
        self.lblCategoryValue.text = categoryData.caseDetails?.category?.name
        self.lblProviderValue.text = quotationData.provider?.providerName
        self.lblQuotationDetailTitle.text = categoryData.caseDetails?.category?.name
        self.lblQuotationDetailValue.text = quotationData.extraNotes
        self.lblOffredPriceValue.text = "\(quotationData.price ?? "")"+"KD_key".localized
        
        self.lblName.text = preferredLanguage == "en" ? "\("Name_key".localized) \(getUserData()?.user?.fullname ?? "")" : "\(getUserData()?.user?.fullname ?? "") \("Name_key".localized)"
//        self.lblAddress.text = preferredLanguage == "en" ? "\("Address_key".localized)"+"\(addressData.building), street \(addressData.street), block \(addressData.block)" : "\(addressData.building), street \(addressData.street), block \(addressData.block)"+"\("Address_key".localized)"
        
        self.lblAddress.text = ""
        bntConfirm.isHidden = true
        self.vwAcceptReject.isHidden = false
    }
    
    func setUpData(categoryData: CategoryDataModel, theController: InvoiceVC) {
        
        self.tblDescription.reloadData()
        
        theController.mainModelView.arrayImages = (categoryData.caseDetails?.fields?.filter({ (data) -> Bool in
            if data.type == "image" {
                vwImages.isHidden = false
                self.collectionImages.reloadData()
                return true
            }
            return false
        }))!
        self.pageController.numberOfPages = theController.mainModelView.arrayImages.first?.value?.count ?? 0
        
        self.lblRequestValue.text = "\(categoryData.caseDetails?.caseId ?? 0)"
        self.lblCategoryValue.text = categoryData.caseDetails?.category?.name
        self.lblProviderValue.text = categoryData.quotationDetails?.first?.provider?.providerName
//            quotationData.provider?.providerName
        self.lblQuotationDetailTitle.text = categoryData.caseDetails?.category?.name
        self.lblQuotationDetailValue.text = categoryData.quotationDetails?.first?.extraNotes
        self.lblOffredPriceValue.text = "\(categoryData.quotationDetails?.first?.price ?? "")"+"KD_key".localized
        self.lblName.text = getUserData()?.user?.fullname
        self.lblAddress.text = categoryData.userDetails?.address ?? ""
        
        print("statu id:", categoryData.caseDetails?.statusId ?? 0)
        if categoryData.caseDetails?.statusId == 1 {
            self.btnTrackNow.isHidden = true
            self.btnCompletedJob.isHidden = true
        }
        else if categoryData.caseDetails?.statusId == 4 || categoryData.caseDetails?.statusId == 5 {
            self.btnTrackNow.isHidden = false
        }
        else if categoryData.caseDetails?.statusId == 6 {
            self.btnTrackNow.isHidden = false
            self.btnCompletedJob.isHidden = false
        }
        else if categoryData.caseDetails?.statusId == 7 {
            self.btnTrackNow.isHidden = true
            self.btnCompletedJob.isHidden = true
        }
        
        /*
        if categoryData.caseDetails?.status?.lowercased() == acceptedCase {
            self.btnTrackNow.isHidden = true
            self.btnCompletedJob.isHidden = true
        }
        else if categoryData.caseDetails?.status?.lowercased() == startJob || categoryData.caseDetails?.status?.lowercased() == tracking {
            self.btnTrackNow.isHidden = false
        }
        else if categoryData.caseDetails?.status?.lowercased() == arrivedAtLocation {
            self.btnTrackNow.isHidden = false
            self.btnCompletedJob.isHidden = false
        }
        else if categoryData.caseDetails?.status?.lowercased() == userConfirm {
            self.btnTrackNow.isHidden = true
            self.btnCompletedJob.isHidden = true
        }*/
    }
    
    func timerAction(max: Float, min: Float){
        
        indicaterAudio.maximumValue = max
        indicaterAudio.minimumValue = 0
        indicaterAudio.layoutIfNeeded()
        
        indicaterAudio.setValue(min, animated: false)
    }
}
