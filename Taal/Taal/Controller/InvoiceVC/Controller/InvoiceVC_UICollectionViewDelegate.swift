//
//  InvoiceVC_UICollectionViewDelegate.swift
//  Taal
//
//  Created by Vishal on 24/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

extension InvoiceVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.mainModelView.arrayImages.first?.value?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InvoiceCollectionCell", for: indexPath) as! InvoiceCollectionCell
        
        let img = self.mainModelView.arrayImages.first?.value?[indexPath.row]
        
        cell.imgView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imgView.sd_setImage(with: img?.toURL(), placeholderImage: UIImage(named: "ic_request_page_splash_holder"), options: .lowPriority, context: nil)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.mainView.pageController.currentPage = indexPath.row
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
}
