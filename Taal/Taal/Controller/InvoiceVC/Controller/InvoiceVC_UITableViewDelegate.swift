//
//  InvoiceVC_UITableViewDelegate.swift
//  Taal
//
//  Created by Vishal on 23/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

extension InvoiceVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let filter = self.mainModelView.dicAcceptedData?.caseDetails?.fields?.filter{ (dict) -> Bool in
            let voice = dict.type?.lowercased() != "Voice".lowercased()
            let img = dict.type != "image"
            return voice && img
        }
        return filter?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionTableCell", for: indexPath) as! DescriptionTableCell
        
        let filter = self.mainModelView.dicAcceptedData?.caseDetails?.fields?.filter{ (dict) -> Bool in
            let voice = dict.type?.lowercased() != "Voice".lowercased()
            let img = dict.type != "image"
            return voice && img
        }
        
        let dict = filter?[indexPath.row]
        cell.lblDescriptionTitle.text = "\(dict?.label ?? "")"+": "
        cell.lblDescriptionValue.text = dict?.value?.first ?? ""
        
        return cell
    }
}
