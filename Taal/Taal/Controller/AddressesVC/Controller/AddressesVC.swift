//
//  AddressesVC.swift
//  Taal
//
//  Created by Vishal on 28/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit

class AddressesVC: UIViewController {

    //MARK: Variables
    lazy var mainView: AddressesView = { [unowned self] in
        return self.view as! AddressesView
    }()
    
    lazy var mainViewModel: AddressesViewModel = {
        return AddressesViewModel(theController: self)
    }()
    
    //MARK: Controller life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainView.setUpUI(theController: self)
        self.setUpNavigationBarWithTitleRightAndBack(strTitle: "Addresses_key".localized, barColor: .appThemeBlueColor, showTitle: true, isLeftBackButton: true, isRightSkipButton: false)
        self.mainViewModel.getAllAddress { 
            self.mainView.tblAddresses.reloadData()
        }
        setUpPullToRefresh()
        
        let rightSkipButton = UIBarButtonItem(image: UIImage(named: "ic_header_add"), style: .plain, target: self, action: #selector(rightSkipButtonAction))
        
        rightSkipButton.setTitleTextAttributes([NSAttributedString.Key.font : themeFont(size: 14, fontname: .regular)], for: .normal)
        
        rightSkipButton.tintColor = UIColor.white
        navigationItem.rightBarButtonItem = rightSkipButton
        
    }
    
    
    @objc func rightSkipButtonAction() {
       let addAddressVC = AppStoryboard.Profile.instance.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
        addAddressVC.mainViewModel.addAddressHandlor = {
            self.mainViewModel.getAllAddress {
                self.mainView.tblAddresses.reloadData()
            }
        }
        addAddressVC.mainViewModel.checkAddressTitle = .addAddress
        self.navigationController?.pushViewController(addAddressVC, animated: true)
    }
}


//MARK: SetUp
extension AddressesVC {
    
    func setUpPullToRefresh() {
        
        self.mainViewModel.refreshController = UIRefreshControl()
        self.mainViewModel.refreshController.backgroundColor = UIColor.clear
        self.mainViewModel.refreshController.tintColor = UIColor.appThemeBlueColor
        self.mainView.tblAddresses.addSubview(self.mainViewModel.refreshController)
        self.mainViewModel.refreshController.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
    }
    
    @objc func pullToRefresh() {
        self.mainViewModel.getAllAddress {
            self.mainView.tblAddresses.reloadData()
        }
    }
}
