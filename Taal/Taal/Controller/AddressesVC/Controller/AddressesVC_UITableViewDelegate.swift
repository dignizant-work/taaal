//
//  AddressesVC_UITableViewDelegate.swift
//  Taal
//
//  Created by Vishal on 28/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwipeCellKit

extension AddressesVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.mainViewModel.arrayAllAddress.count == 0 {
            setTableView("No_record_found_key".localized, tableView: self.mainView.tblAddresses)
            return 0
        }
        
        tableView.backgroundView = nil
        return self.mainViewModel.arrayAllAddress.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressesTableCell", for: indexPath) as! AddressesTableCell
        
        let dict = self.mainViewModel.arrayAllAddress[indexPath.row]
        cell.setUpData(dict: dict)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.mainViewModel.enumAddressCheck == .addCase {
            let vc = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "QuotationAcceptedVC") as! QuotationAcceptedVC
            
            let dict = self.mainViewModel.arrayAllAddress[indexPath.row]
            self.mainViewModel.dictAddCaseData["address_id"].stringValue = "\(dict.id!)"
            
            vc.isCheckController = .addCase
            
            self.mainViewModel.addCaseAPI(param: self.mainViewModel.dictAddCaseData.dictionaryObject!) {
                print("Case Add")
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else if self.mainViewModel.enumAddressCheck == .acceptedQuotation {
            
            let dict = self.mainViewModel.arrayAllAddress[indexPath.row]
            
            showAlert("", "Quotation accept with this address?", "Accept", "Cancel") {
                
                let vc = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "QuotationAcceptedVC") as! QuotationAcceptedVC
                
                let param = ["case_id":"\(self.mainViewModel.dicAcceptedData?.caseDetails?.caseId ?? 0)",
                    "provider_id":"\(self.mainViewModel.dicQuotationDetails?.provider?.providerId ?? 0)",
                    "quotation_id":"\(self.mainViewModel.dicQuotationDetails?.id ?? 0)",
                    "address_id":"\(dict.id ?? 0)"]
                
                self.mainViewModel.quotationAcceptedAPI(param: param) {
                    
                    if vc.isCheckController == .addCase {
                        vc.isCheckController = .addCase
                    }
                    else {
                        vc.isCheckController = .acceptedQuotation
                    }
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
//            let dict = self.mainViewModel.arrayAllAddress[indexPath.row]
//            let invoiceVC = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "InvoiceVC") as! InvoiceVC
//            invoiceVC.mainModelView.enumNavigationTitle = .acceptedQuotation
//            invoiceVC.mainModelView.dictAddressData = dict
//            invoiceVC.mainModelView.dicAcceptedData = self.mainViewModel.dicAcceptedData
//            invoiceVC.mainModelView.dicQuotationDetails = self.mainViewModel.dicQuotationDetails
//            self.navigationController?.pushViewController(invoiceVC, animated: true)
        }
        else if self.mainViewModel.enumAddressCheck == .isEditCase {
            
            let vc = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "QuotationAcceptedVC") as! QuotationAcceptedVC
            vc.isCheckController = .isEditCase
            let dict = self.mainViewModel.arrayAllAddress[indexPath.row]
            self.mainViewModel.dictAddCaseData["address_id"].stringValue = "\(dict.id!)"
            
            let caseId = self.mainViewModel.dictAddCaseData["category_id"].stringValue
            
            self.mainViewModel.dictAddCaseData.dictionaryObject?.removeValue(forKey: "category_id")
            
            self.mainViewModel.updateCaseAPI(caseID: caseId, param: self.mainViewModel.dictAddCaseData.dictionaryObject!) {
                print("Case update")
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let edit = UIContextualAction(style: .normal, title: "Edit_key".localized) { (contex, view, success:(Bool) -> Void) in
            
            let addAddressVC = AppStoryboard.Profile.instance.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
            
            addAddressVC.mainViewModel.dictAddressData = self.mainViewModel.arrayAllAddress[indexPath.row]
            addAddressVC.mainViewModel.addAddressHandlor = {
                self.mainViewModel.getAllAddress {
                    self.mainView.tblAddresses.reloadData()
                }
            }
            addAddressVC.mainViewModel.checkAddressTitle = .editAddress
            self.navigationController?.pushViewController(addAddressVC, animated: true)
            
            success(true)
        }
        edit.image = UIImage(named: "ic_edit")
        edit.backgroundColor = .red
        
        let delete = UIContextualAction(style: .normal, title: "Delete_key".localized) { (contex, view, success:(Bool) -> Void) in
            
            let dict = self.mainViewModel.arrayAllAddress[indexPath.row]
            
            if dict.id != nil {
                self.mainViewModel.deleteAddressApi(addressId: "\(dict.id!)") {
                    self.mainViewModel.arrayAllAddress.remove(at: indexPath.row)
                    self.mainView.tblAddresses.reloadData()
                }
            }
            success(true)
        }
        delete.image = UIImage(named: "ic_delete")
        delete.backgroundColor = .red
        
        return UISwipeActionsConfiguration(actions: [delete, edit])
    }
}
