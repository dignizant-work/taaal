//
//  AddAddressVC_UITextFeidDelegates.swift
//  Taal
//
//  Created by Abhay on 02/12/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import DropDown

extension AddAddressVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
                
        if textField == mainView.txtCountry {
            self.view.endEditing(true)
            self.mainViewModel.countriDropDown.show()
            return false
        }
        else if textField == mainView.txtGovernorate {
            self.view.endEditing(true)
            self.mainViewModel.zoneDropDown.show()
            return false
        }
        else if textField == mainView.txtCity {
            self.view.endEditing(true)
            self.mainViewModel.citiDropDown.show()
            return false
        }
        
        return true
    }
    
}
