//
//  AddAddressVC.swift
//  Taal
//
//  Created by Abhay on 29/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON

class AddAddressVC: UIViewController {

    //MARK: Variables
    lazy var mainView: AddAddressView = { [unowned self] in
        return self.view as! AddAddressView
        }()
    
    lazy var mainViewModel: AddAddressesViewModel = {
        return AddAddressesViewModel(theController: self)
    }()
    
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.checkController()
        
        self.showActivityIndicatory(textField: self.mainView.txtCountry)
        
        if self.mainViewModel.checkAddressTitle == .editAddress {
            self.mainView.setUPData(theController: self, data: self.mainViewModel.dictAddressData!)
        }
        
        self.mainViewModel.getAllCountriesAPI(txtField: mainView.txtCountry) {
            
            self.setUpCountryDropDown()
            
            self.showActivityIndicatory(textField: self.mainView.txtGovernorate)
            
            var countryID = getUserData()?.user?.countryId ?? 0
            self.mainView.setUPData(theController: self, data: self.mainViewModel.dictAddressData!)
            
            if self.mainViewModel.checkAddressTitle == .editAddress {
                countryID = self.mainViewModel.dictAddressData?.countryId ?? 0
            }
            
            self.mainViewModel.getAllZonesAPI(countryID: "\(countryID)", txtField: self.mainView.txtGovernorate) {
                
                self.setUpZoneDropDown()
                self.mainView.setUPData(theController: self, data: self.mainViewModel.dictAddressData!)
                
                self.showActivityIndicatory(textField: self.mainView.txtCity)
                self.mainViewModel.getAllCitiesAPI(countryID: "\(countryID)", txtField: self.mainView.txtCity) {
                    self.setUpCityDropDown()
                    
                    if self.mainViewModel.checkAddressTitle == .editAddress {
                        if self.mainViewModel.dictAddressData != nil {
                            self.mainView.setUPData(theController: self, data: self.mainViewModel.dictAddressData!)
                        }
                    }
                }
            }
        }
        
        self.mainView.setUpUI(theController: self)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
}

//MARK: Button action
extension AddAddressVC {
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        if (self.mainView.txtAddressName.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            makeToast(strMessage: "Please_enter_address_name_key".localized)
        }
        else if (self.mainView.txtCountry.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            makeToast(strMessage: "Please_select_country_key".localized)
        }
        else if (self.mainView.txtGovernorate.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            makeToast(strMessage: "Please_select_governorate_key".localized)
        }
        else if (self.mainView.txtCity.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            makeToast(strMessage: "Please_select_city_key".localized)
        }
        else if (self.mainView.txtAreaName.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            makeToast(strMessage: "Please enter area name".localized)
        }
        else if (self.mainView.txtBlock.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            makeToast(strMessage: "Please_enter_block_key".localized)
        }
        else if (self.mainView.txtStreet.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            makeToast(strMessage: "Please_enter_street_key".localized)
        }
//        else if (self.mainView.txtAvenu.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
//            makeToast(strMessage: "Please_enter_avenue_key".localized)
//        }
        else if (self.mainView.txtBuilding.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            makeToast(strMessage: "Please_enter_building_key".localized)
        }
//        else if (self.mainView.txtFloor.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
//            makeToast(strMessage: "Please_enter_floor_key".localized)
//        }
//        else if (self.mainView.txViewExtraNotes.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
//            makeToast(strMessage: "Please_enter_extranote_key".localized)
//        }
        else {
            
            var param = ["name":self.mainView.txtAddressName.text!,
            "floor":self.mainView.txtFloor.text ?? "",
            "block":self.mainView.txtBlock.text!,
            "street":self.mainView.txtStreet.text!,
            "area":self.mainView.txtAvenu.text ?? "",
            "building":self.mainView.txtBuilding.text!,
            "area_name":self.mainView.txtAreaName.text ?? "",
            "city_id":self.mainViewModel.cityID,
            "zone_id":self.mainViewModel.zoneID,
            "country_id":self.mainViewModel.countryID,
            "notes":self.mainView.txViewExtraNotes.text ?? ""] as [String : Any]
            
            if self.mainViewModel.checkAddressTitle == .editAddress {
                param["address_id"] = self.mainViewModel.dictAddressData?.id!
                print("PARAM:-", param)
                
                self.mainViewModel.editAddressAPI(param: param) {
                    self.mainViewModel.addAddressHandlor()
                    self.goBack()
                }
            }
            else {
                print("PARAM:-", param)
                self.mainViewModel.addAddressAPI(param: param) {
                    self.mainViewModel.addAddressHandlor()
                    self.goBack()
                }
            }
        }
    }
}

//MARK: SetUp Data in dropDown
extension AddAddressVC {
    
    func checkController() {
        
        if self.mainViewModel.checkAddressTitle == .editAddress {
            setUpNavigationBarWithTitleRightAndBack(strTitle: "EditAddress_key".localized, barColor: UIColor.appThemeBlueColor, showTitle: true, isLeftBackButton: true, isRightSkipButton: false)
        }
        else {
            setUpNavigationBarWithTitleRightAndBack(strTitle: "AddAddress_key".localized, barColor: UIColor.appThemeBlueColor, showTitle: true, isLeftBackButton: true, isRightSkipButton: false)
        }
    }
    
    func setUpCountryDropDown() {
        
        var data : [String] = []
        for i in self.mainViewModel.arrayAllCountries {
            data.append(i.name)
        }
        
        self.mainViewModel.countriDropDown.customCellConfiguration = { index, string, cell in
            cell.optionLabel.textAlignment = .left
        }
        self.mainViewModel.countriDropDown.dataSource = data
        
        self.configuDropDown(dropDown: self.mainViewModel.countriDropDown, sender: self.mainView.txtCountry)
        self.mainViewModel.countriDropDown.selectionAction =  { (index, item) in
            self.mainView.txtCountry.text = item
            self.view.endEditing(true)
            
            [self.mainView.txtGovernorate, self.mainView.txtCity].forEach { (txt) in
                txt?.text = ""
            }
            
            self.mainViewModel.countryID = self.mainViewModel.arrayAllCountries[index].id!
            let countryID = self.mainViewModel.arrayAllCountries[index].id
            
            self.showActivityIndicatory(textField: self.mainView.txtGovernorate)
            self.mainViewModel.getAllZonesAPI(countryID: "\(countryID!)", txtField: self.mainView.txtGovernorate) {
                self.setUpZoneDropDown()
                
                self.showActivityIndicatory(textField: self.mainView.txtCity)
                self.mainViewModel.getAllCitiesAPI(countryID: "\(countryID!)", txtField: self.mainView.txtCity) {
                    self.setUpCityDropDown()
                }
            }
            self.mainViewModel.countriDropDown.hide()
        }
    }
    
    func setUpZoneDropDown() {
        
        var data : [String] = []
        for i in self.mainViewModel.arrayAllZones {
            data.append(i.name)
        }
        
        self.mainViewModel.zoneDropDown.customCellConfiguration = { index, string, cell in
            cell.optionLabel.textAlignment = .left
        }
        self.mainViewModel.zoneDropDown.dataSource = data
        
        self.configuDropDown(dropDown: self.mainViewModel.zoneDropDown, sender: self.mainView.txtGovernorate)
        self.mainViewModel.zoneDropDown.selectionAction =  { (index, item) in
            self.mainView.txtGovernorate.text = item
            self.mainViewModel.zoneID = self.mainViewModel.arrayAllZones[index].id!
            self.view.endEditing(true)
            self.mainViewModel.zoneDropDown.hide()
        }
    }
    
    func setUpCityDropDown() {
        
        var data : [String] = []
        for i in self.mainViewModel.arrayAllCities {
            data.append(i.name)
        }
        
        self.mainViewModel.citiDropDown.customCellConfiguration = { index, string, cell in
            cell.optionLabel.textAlignment = .left
        }
        self.mainViewModel.citiDropDown.dataSource = data
        
        self.configuDropDown(dropDown: self.mainViewModel.citiDropDown, sender: self.mainView.txtCity)
        self.mainViewModel.citiDropDown.selectionAction =  { (index, item) in
            self.mainView.txtCity.text = item
            self.mainViewModel.cityID = self.mainViewModel.arrayAllCities[index].id!
            self.view.endEditing(true)
            self.mainViewModel.citiDropDown.hide()
        }
    }
}
