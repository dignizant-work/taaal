//
//  AddAddressViewModel.swift
//  Taal
//
//  Created by Abhay on 29/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import DropDown
import SwiftyJSON


class AddAddressesViewModel {
    
    fileprivate weak var theController : AddAddressVC!
//    var cityNameDropDown = DropDown()
    var addAddressHandlor: ()->Void = {}
    var dictAddressData = AddressDataModel(JSON: JSON().dictionaryObject!)
    var arrayCityName : [String] = []
    var checkAddressTitle = enumAddressTitle.addAddress
    var arrayAllCountries : [AddressDataModel] = []
    var arrayAllZones : [AddressDataModel] = []
    var arrayAllCities : [AddressDataModel] = []
    
    var countriDropDown = DropDown()
    var zoneDropDown = DropDown()
    var citiDropDown = DropDown()
    
    var countryID = NSNumber()
    var cityID = NSNumber()
    var zoneID = NSNumber()
    
    
    //MARK: Initializer
    init(theController: AddAddressVC) {
        self.theController = theController
    }
    
}


//MARK: API Setup
extension AddAddressesViewModel {
    
    func getAllCountriesAPI(txtField: UITextField, completionHandlor:@escaping()->Void) {
        
        let url = getAllCountriesURL
        print("URL: ", url)
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        print("Header: ", header)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        WebServices().MakeGetAPI(name: url, params: [:], header: header) { (response, error, statusCode) in
            self.theController.stopActivityIndicatory(textField: txtField)
            
            if statusCode == success {
                
                if let array = response as? NSArray {
                    let jsonArray = JSON(array).arrayValue
//                    print("Country Array: ", jsonArray)
                    self.arrayAllCountries = []
                    for data in jsonArray {
                        let value = AddressDataModel(JSON: data.dictionaryObject!)
                        self.arrayAllCountries.append(value!)
                    }
                }
                completionHandlor()
            }
            else if statusCode == notFound {
                
            }
            else if error != nil {
                print("Eror: ", error?.localized ?? "")
                makeToast(strMessage: error?.localized  ?? "")
                completionHandlor()
            }
            else {
                print("StatusCode: ", statusCode ?? 0)
            }
        }
    }
    
    func getAllZonesAPI(countryID: String, txtField: UITextField, completionHandlor:@escaping()->Void) {
        
        let url = getAllZonesURL+countryID
        print("URL: ", url)
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
//        print("Header: ", header)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        WebServices().MakeGetAPI(name: url, params: [:], header: header) { (response, error, statusCode) in
            self.theController.stopActivityIndicatory(textField: txtField)
            
            if statusCode == success {
                if let array = response as? NSArray {
                    let jsonArray = JSON(array).arrayValue
//                    print("Zone Array: ", jsonArray)
                    self.arrayAllZones = []
                    for data in jsonArray {
                        let addData = AddressDataModel(JSON: data.dictionaryObject!)
                        self.arrayAllZones.append(addData!)
                    }
                }
                completionHandlor()
            }
            else if statusCode == notFound {
                
            }
            else if error != nil {
                print("Error: ", error?.localized ?? "")
                makeToast(strMessage: error?.localized ?? "")
                completionHandlor()
            }
            else {
                
            }
        }
    }
    
    func getAllCitiesAPI(countryID: String, txtField: UITextField, completionHandlor:@escaping()->Void) {
        
        let url = getAllCitiesURL+countryID
        print("URL: ", url)
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
//        print("Header: ", header)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        WebServices().MakeGetAPI(name: url, params: [:], header: header) { (response, error, statusCode) in
            self.theController.stopActivityIndicatory(textField: txtField)
            
            if statusCode == success {
                if let array = response as? NSArray {
//                    print("Array: ", array)
                    let jsonArray = JSON(array).arrayValue
//                    print("City Array: ", jsonArray)
                    self.arrayAllCities = []
                    for data in jsonArray {
                        let addData = AddressDataModel(JSON: data.dictionaryObject!)
                        self.arrayAllCities.append(addData!)
                    }
                }
                completionHandlor()
            }
            else if statusCode == notFound {
                
            }
            else if error != nil {
                print("Error: ", error?.localized ?? "")
                makeToast(strMessage: error?.localized ?? "")
                completionHandlor()
            }
            else {
                
            }
        }
    }
    
    
    func addAddressAPI(param:[String:Any], completionHandlor:@escaping()->Void) {
        
        let url = addAddressURL
        print("URL:-", url)
        print("PARAM:-", param)
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        print("Header: ", header)
        
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        self.theController.showLoader()
        WebServices().MakePostAPI(name: url, params: param, header: header) { (response, error, statusCode) in
            
            self.theController.hideLoader()
            if statusCode == success {
                if let data = response {
                    print("Data:-",data)
                    completionHandlor()
                }
                else if statusCode == notFound {
                    completionHandlor()
                }
                else if error != nil {
                    makeToast(strMessage: error?.localized ?? "")
                }
                else {
                    
                }
            }
        }
    }
    
    func editAddressAPI(param:[String:Any], completionHandlor:@escaping() -> Void) {
        
        let url = editAddressURL
        print("URL:-", url)
        print("PARAM:-", param)
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        print("Header: ", header)
        
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        self.theController.showLoader()
        WebServices().MakePutAPI(name: url, params: param, header: header) { (response, error, statusCode) in
            
            self.theController.hideLoader()
            if statusCode == success {
                if let data = response as? NSDictionary {
                    print("Data:-",data)
                    completionHandlor()
                }
                else if let array = response as? NSArray {
                    print("Array:-",array)
                    completionHandlor()
                }
            }
            else if statusCode == notFound {
                completionHandlor()
            }
            else if error != nil {
                makeToast(strMessage: error?.localized ?? "")
            }
            else {
                
            }
        }
    }
}
