//
//  AddControllerViewModel.swift
//  Taal
//
//  Created by Vishal on 16/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import DropDown

class AddCategoryViewModel {
    
    fileprivate let theController: AddCategoryVC!
    
    init(theController: AddCategoryVC) {
        self.theController = theController
    }
    
    var caseID = ""
    var dictCategoryValue = CategoryDataModel(JSON: JSON().dictionaryObject!)
    var arrayCaseData : [CategoryDataModel] = []
    var selectionDropDown = DropDown()
    let recordView = RecordView()
    var arrayImages: NSArray! = []
    var handlorAddImage: () -> Void = {}
    var handlorDeleteImage: (_ collectionTag: Int) -> Void = {_ in}
    var arrData = [JSON]()
    var dictAddCase = JSON()
    var handlorDropDown:(_ index: Int,_ text: String)->Void = { _, _ in }
    var handlorCheckMark:(_ index: Int)->Void = { _ in }
    var enumAddCase = enumAddCaseVcCheck.addCase
    
}

extension AddCategoryViewModel {
    
    func getCaseParameters(categoryID:String, completionHandlor:@escaping() -> Void) {
        
        let url = getRequestParametersURL+categoryID
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        
        print("URL: ", url)
        print("Header: ", header)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        self.theController.showLoader()
        WebServices().MakeGetAPI(name: url, params: [:], header: header) { (response, error, statusCode) in
            self.theController.hideLoader()
            
            if statusCode == success {
//                print("Response: ", response!)
                if let array = response as? NSArray {
                    var jsonArray = JSON(array).arrayValue
                    
                    self.arrayCaseData = []
                    for (index, data) in jsonArray.enumerated() {
                        
                        jsonArray[index]["valuePassed"].boolValue = false
                        jsonArray[index]["enterValue"].stringValue = ""
                        let value = CategoryDataModel(JSON: JSON(jsonArray[index]).dictionaryObject!)
                        self.arrayCaseData.append(value!)
                    }
                }
                completionHandlor()
            }
            else if statusCode == notFound {
                completionHandlor()
            }
            else if error != nil {
                print("Error: ", error!)
                makeToast(strMessage: error?.localized ?? "")
                completionHandlor()
            }
        }
    }
    
    func editCaseGetData(caseId: String, completionHandlor:@escaping()->Void) {
        
        let url = editCaseURL+caseId
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        
        print("URL: ", url)
        print("Header: ", header)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        self.theController.showLoader()
        WebServices().MakeGetAPI(name: url, params: [:], header: header) { (response, error, statusCode) in
            self.theController.hideLoader()
            
            if statusCode == success {
                print("Response: ", response)
                if let array = response as? NSArray {
                    var jsonArray = JSON(array).arrayValue
                    
                    self.arrayCaseData = []
                    for (index, data) in jsonArray.enumerated() {
                        jsonArray[index]["valuePassed"].boolValue = data["required"].boolValue
                        jsonArray[index]["enterValue"].stringValue = ""
                        let value = CategoryDataModel(JSON: JSON(jsonArray[index]).dictionaryObject!)
                        
                        if jsonArray[index].dictionaryValue["type"]!.stringValue == "image" {
                            let imgArray = jsonArray[index].dictionaryValue["edit_value"]!.arrayObject!
                            for img in imgArray {
//                                print("Img: ", img)
                                let url = URL(string:"\(img)")
                                if let data = try? Data(contentsOf: url!) {
                                    let image: UIImage = UIImage(data: data)!
                                    value?.arrayImage.append(image)
                                }
                            }
                        }
//                        print("Count: ", value?.arrayImage.count ?? 0)
                        self.arrayCaseData.append(value!)
                    }
                }
                completionHandlor()
            }
            else if statusCode == notFound {
                completionHandlor()
            }
            else if error != nil {
                print("Error: ", error!)
                makeToast(strMessage: error?.localized ?? "")
                completionHandlor()
            }
        }
    }
}

