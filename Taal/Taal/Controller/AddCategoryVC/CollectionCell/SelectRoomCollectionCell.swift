//
//  SelectRoomCollectionCell.swift
//  Taal
//
//  Created by Vishal on 24/06/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class SelectRoomCollectionCell: UICollectionViewCell {

    @IBOutlet weak var btnTotalRoom: UIButton!
    var radioHandlor:(Int, String, UICollectionView)-> Void = { _,_, _ in }
    var currentCollection: UICollectionView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        btnTotalRoom.isSelected = false
    }
    
    @IBAction func btnRadioSelectionAction(_ sender: UIButton) {
        self.radioHandlor(sender.tag,"a" ,currentCollection)
        btnTotalRoom.isSelected = true
    }
}
