//
//  AddCategoryVC_TableViewDelegate.swift
//  Taal
//
//  Created by Vishal on 16/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

extension AddCategoryVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.mainView.tblAddCategory {
            
            if self.mainModelView.arrayCaseData.count > 0 {
                return self.mainModelView.arrayCaseData.count
            }
            return 0
        }
        
        var count = Int()
        for data in self.mainModelView.arrayCaseData {
            if data.type == "checkbox-group" {
                count = data.value?.count ?? 0
            }
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.mainView.tblAddCategory {
            
            let dict = self.mainModelView.arrayCaseData[indexPath.row]
            
            if dict.type == "voice" {
                let audioCell = tableView.dequeueReusableCell(withIdentifier: "AudioTableCell", for: indexPath) as! AudioTableCell
//                audioCell.lblAudio.text =  dict.required == true ? dict.label!+"*" : dict.label!
                audioCell.lblAudio.text =  dict.required == true ? dict.label!+"*" : dict.label!
                audioCell.btnAudio.tag = indexPath.row
                
                var txtData = JSON()
                if dict.editValue?.count ?? 0 > 0 {
                    
                    txtData["id"].int = Int(truncating: dict.id!)
                    txtData["type"].stringValue = dict.type!
                    let base64Data = Data((dict.editValue?.first ?? "").utf8).base64EncodedString()
                    txtData["value"] = [base64Data]
                    self.mainModelView.arrData.append(txtData)
                    audioCell.lblAudio.text = audioCell.fileName
                    print("")
                }
                audioCell.audioHandlor = { index, text in
                    print("Index: ", index)
                    print("Audio str:", text)
                    
                    self.mainModelView.arrayCaseData[index].valuePassed = true
                    self.mainModelView.arrayCaseData[index].enterValue = text
                    
                    dict.valuePassed = true
                    
                    let filter = self.mainModelView.arrData.filter { (data) -> Bool in
                        let data = data["id"].int == Int(truncating: dict.id!) ? true : false
                        return data
                    }
                    
                    if filter.count == 0 {
                        txtData["id"].int = Int(truncating: dict.id!)
                        txtData["type"].stringValue = dict.type!
                        txtData["value"] = [text]
                        self.mainModelView.arrData.append(txtData)
                    }
                    
                    for i in 0..<self.mainModelView.arrData.count {
                        if self.mainModelView.arrData[i]["id"].int == Int(truncating: dict.id!) {
                            self.mainModelView.arrData[i]["value"] = [text]
                        }
                    }
                }
                return audioCell
            }
            else if dict.type == "text" || dict.type == "textarea" {
                let txtCell = tableView.dequeueReusableCell(withIdentifier: "txtFieldTableCell", for: indexPath) as! txtFieldTableCell
                
                let data = txtCell.txtValue.text
                dict.value?.append(data!)
                
                if dict.editValue?.count ?? 0 > 0 {
                    var txtData = JSON()
                    txtData["id"].int = Int(truncating: dict.id!)
                    txtData["type"].stringValue = dict.type!
                    txtData["value"] = [dict.editValue?.first ?? ""]
                    self.mainModelView.arrData.append(txtData)
                    print("")
                }
                
                txtCell.setUpData(data: dict)
                txtCell.txtValue.tag = indexPath.row
                
                txtCell.handlorTxtValue = { index, text, isEditOrNot  in
                    self.mainModelView.arrayCaseData[index].valuePassed = true
                    self.mainModelView.arrayCaseData[index].enterValue = text
                    
                    if isEditOrNot {
                        for i in 0..<self.mainModelView.arrData.count {
                            if self.mainModelView.arrData[i]["id"].int == Int(truncating: dict.id!) {
                                self.mainModelView.arrData[i]["value"] = [text]
                            }
                        }
                    }
                    else {
                        var txtData = JSON()
                        txtData["id"].int = Int(truncating: dict.id!)
                        txtData["type"].stringValue = dict.type!
                        txtData["value"] = [text]
                        self.mainModelView.arrData.append(txtData)
                        
                    }
                }
                
                return txtCell
            }
            else if dict.type == "radio-group" {
                
                let imgCell = tableView.dequeueReusableCell(withIdentifier: "ImgCollectionTableCell", for: indexPath) as! ImgCollectionTableCell
                
                imgCell.lblImagesHeader.text =  dict.required == true ? dict.label!+"*" : dict.label!
                
                imgCell.imgAddCollection.tag = indexPath.row
                imgCell.imgAddCollection.delegate = self
                imgCell.imgAddCollection.dataSource = self
                imgCell.imgAddCollection.reloadData()
                imgCell.imgAddCollection.layoutIfNeeded()
                imgCell.imgAddCollection.layoutSubviews()
                
                return imgCell
                
               /* let submitcell = tableView.dequeueReusableCell(withIdentifier: "GenderTableCell", for: indexPath) as! GenderTableCell
                
                submitcell.setUpData(data: dict)
                submitcell.btnMale.tag = indexPath.row
                submitcell.btnFemale.tag = indexPath.row
//                submitcell.lblTitle.text =  dict.required == true ? dict.label!+"*" : dict.label!
                submitcell.lblTitle.text =  dict.required == true ? "Do you prefer Male or Female?".localized+"*" : "Do you prefer Male or Female?".localized
                
                submitcell.handlorTxtValue = { index, text in
                    
                    self.mainModelView.arrayCaseData[index].valuePassed = true
                    self.mainModelView.arrayCaseData[index].enterValue = text
                    
                    var txtData = JSON()
                    txtData["id"].int = Int(truncating: dict.id!)
                    txtData["type"].stringValue = dict.type!
                    txtData["value"] = [text]
                    self.mainModelView.arrData.append(txtData)
                }
                
                return submitcell */
            }
            else if dict.type == "image" || dict.type == "images" {
                let imgCell = tableView.dequeueReusableCell(withIdentifier: "ImgCollectionTableCell", for: indexPath) as! ImgCollectionTableCell
                
                tableTag = indexPath.row
                imgCell.lblImagesHeader.text =  dict.required == true ? dict.label!+"*" : dict.label!
                
                imgCell.imgAddCollection.tag = indexPath.row
                imgCell.imgAddCollection.delegate = self
                imgCell.imgAddCollection.dataSource = self
                imgCell.imgAddCollection.reloadData()
                imgCell.imgAddCollection.layoutIfNeeded()
                imgCell.imgAddCollection.layoutSubviews()
                
                return imgCell
            }
            else if dict.type == "select" {
                let ddCell = tableView.dequeueReusableCell(withIdentifier: "DropdownTableCell", for: indexPath) as! DropdownTableCell
                
                if dict.editValue?.count ?? 0 > 0 {
                    ddCell.btnCategory.setTitle(dict.editValue?.first ?? "", for: .normal)
                }
                else {
                    ddCell.btnCategory.setTitle(dict.required == true ? dict.label!+"*" : dict.label!, for: .normal)
                }
                
                ddCell.btnCategory.tag = indexPath.row
                self.dropDownSetup(sender: ddCell.btnCategory, dataSource: dict.value!)
                ddCell.btnCategory.addTarget(self, action: #selector(btnSelectionDDAction(_:)), for: .touchUpInside)
                
                var txtData = JSON()
                if dict.editValue?.count ?? 0 > 0 {
                    
                    txtData["id"].int = Int(truncating: dict.id!)
                    txtData["type"].stringValue = dict.type!
                    txtData["value"] = [dict.editValue?.first ?? ""]
                    self.mainModelView.arrData.append(txtData)
                }
                
                self.mainModelView.handlorDropDown = { index, text  in
//                    print("index: ", index)
//                    print("TEXT: ", text)
                    self.mainModelView.arrayCaseData[index].valuePassed = true
                    self.mainModelView.arrayCaseData[index].enterValue = text
                    
                    let filter = self.mainModelView.arrData.filter { (data) -> Bool in
                        let data = data["id"].int == Int(truncating: dict.id!) ? true : false
                        return data
                    }
                    if filter.count == 0 {
                        txtData["id"].int = Int(truncating: dict.id!)
                        txtData["type"].stringValue = dict.type!
                        txtData["value"] = [text]
                        self.mainModelView.arrData.append(txtData)
                    }
                    for i in 0..<self.mainModelView.arrData.count {
                        if self.mainModelView.arrData[i]["id"].int == Int(truncating: dict.id!) {
                            self.mainModelView.arrData[i]["value"] = [text]
                        }
                    }
                }
                
                return ddCell
            }
            else if dict.type == "checkbox-group" {
                let itemCell = tableView.dequeueReusableCell(withIdentifier: "CheckMarkTable", for: indexPath) as! CheckMarkTable
                
                itemCell.lblItemTitle.text =  dict.required == true ? dict.label!+"*" : dict.label!
                
                itemCell.tblItemList.register(UINib(nibName: "ItemCheckMarkTableCell", bundle: nil), forCellReuseIdentifier: "ItemCheckMarkTableCell")
                itemCell.tblItemList.tag = indexPath.row
                itemCell.tblItemList.delegate = self
                itemCell.tblItemList.dataSource = self
                itemCell.tblItemList.reloadData()
                self.checkBoxTableTag = indexPath.row
//                UIView.performWithoutAnimation {
//                    self.mainView.tblAddCategory.reloadData()
//                }
                
                return itemCell
            }
        }
        
        let itemCell = tableView.dequeueReusableCell(withIdentifier: "ItemCheckMarkTableCell", for: indexPath) as! ItemCheckMarkTableCell
        itemCell.btnCheckMark.tag = indexPath.row
        
        itemCell.btnCheckMark.addTarget(self, action: #selector(btnCheckMarkSelected(_:)), for: .touchUpInside)
        
        let dict = self.mainModelView.arrayCaseData[tableView.tag]
        let value = self.mainModelView.arrayCaseData[tableView.tag].value?[indexPath.row]
        itemCell.lblItemValueText.text = value!
        
        var txtData = JSON()
        if dict.editValue?.count ?? 0 > 0 {
            for i in 0..<dict.editValue!.count {
                let data = dict.editValue![i]
                if value == data {
                    itemCell.btnCheckMark.isSelected = true
                    txtData["id"].int = Int(truncating: dict.id!)
                    txtData["type"].stringValue = dict.type!
                    txtData["value"] = [value!]
                    self.mainModelView.arrData.append(txtData)
                }
            }
//            itemCell.btnCheckMark.isSelected = true
        }
        
        self.mainModelView.handlorCheckMark = { index in
            print("index: ", index)
//            self.mainModelView.arrayCaseData[index].valuePassed = true
//            self.mainModelView.arrayCaseData[index].enterValue = value ?? ""
            
            self.mainModelView.arrayCaseData[tableView.tag].valuePassed = true
            self.mainModelView.arrayCaseData[tableView.tag].enterValue = value ?? ""
            
            let checkBoxValue = self.mainModelView.arrayCaseData[self.checkBoxTableTag].value?[index]
            
            let filter = self.mainModelView.arrData.filter { (data) -> Bool in
                let data = data["id"].int == Int(truncating: dict.id!) ? true : false
                return data
            }
            if filter.count == 0 {
                txtData["id"].int = Int(truncating: dict.id!)
                txtData["type"].stringValue = dict.type!
                txtData["value"] = [checkBoxValue!]
                self.mainModelView.arrData.append(txtData)
            }
            for i in 0..<self.mainModelView.arrData.count {
                if self.mainModelView.arrData[i]["id"].int == Int(truncating: dict.id!) {
                    self.mainModelView.arrData[i]["value"] = [checkBoxValue!]
                }
            }
            
        }
        
        return itemCell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let yValue = tableView.contentOffset.y
//        print("Value: ", yValue)
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        let position = scrollView.contentOffset.y
        if position >= 215 {
            
            if self.mainModelView.enumAddCase == .editCase {
                self.setUpNavigationBarWithTitleRightAndBack(strTitle: (self.mainModelView.dictCategoryValue?.caseDetails?.category?.name ?? "").capitalized, barColor: UIColor.appThemeBlueColor, showTitle: true, isLeftBackButton: true, isRightSkipButton: false)
            }
            else {
                self.setUpNavigationBarWithTitleRightAndBack(strTitle: (self.mainModelView.dictCategoryValue?.name ?? "").capitalized, barColor: UIColor.appThemeBlueColor, showTitle: true, isLeftBackButton: true, isRightSkipButton: false)
            }
        }
        else {
            self.title = ""
            setupTransparontsNavigationBar()
        }
    }
}

extension AddCategoryVC {
    
    @objc func btnSelectionDDAction(_ sender:UIButton) {
        self.view.endEditing(true)
        self.mainModelView.selectionDropDown.show()
    }
    
    @objc func btnCheckMarkSelected(_ sender: UIButton) {
        
        if sender.isSelected == true {
            sender.isSelected = false
        }
        else {
            self.mainModelView.handlorCheckMark(sender.tag)
            sender.isSelected = true
        }
    }
}
