//
//  CheckMarkTable.swift
//  Taal
//
//  Created by Vishal on 17/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class CheckMarkTable: UITableViewCell {
    
    //MARK: Outlets
    @IBOutlet weak var lblItemTitle: UILabel!
    @IBOutlet weak var tblItemList: UITableView!
    @IBOutlet weak var tblItemListHeightConstraint: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.tblItemList.register(UINib(nibName: "ItemCheckMarkTableCell", bundle: nil), forCellReuseIdentifier: "ItemCheckMarkTableCell")
        
        tblItemList.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        
        [lblItemTitle].forEach { (lbl) in
            lbl?.textColor = UIColor.lightGray
            lbl?.font = themeFont(size: 17, fontname: .regular)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    //MARK:- Overide Method
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            print("contentSize:= \(tblItemList.contentSize.height)")
            
            self.contentView.layoutIfNeeded()
            self.contentView.layoutSubviews()
            self.tblItemListHeightConstraint.constant = tblItemList.contentSize.height
        }
    }

    deinit {
        tblItemList.removeObserver(self, forKeyPath: "contentSize")
    }
    
}
