//
//  ItemCheckMarkTableCell.swift
//  Taal
//
//  Created by Vishal on 17/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class ItemCheckMarkTableCell: UITableViewCell {

    //MARK: Outlets
    @IBOutlet weak var btnCheckMark: UIButton!
    @IBOutlet weak var lblItemValueText: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
}
