//
//  AudioTableCell.swift
//  Taal
//
//  Created by Vishal on 16/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import AVFoundation

class AudioTableCell: UITableViewCell, RecordViewDelegate {

    //MARK: Outlets
    @IBOutlet weak var vwAudioRecorder: UIView!
    @IBOutlet weak var lblAudio: UILabel!
    @IBOutlet weak var btnAudio: RecordButton!
    var audioHandlor : (_ index: Int, _ audio:String) -> Void = { _,_  in }
    
    let recordView = RecordView()
    var audioRecorder: AVAudioRecorder!
    var audioPlayer: AVAudioPlayer!
    var recordingSession: AVAudioSession!
    var fileName : String = "audio.m4a"
    
    override func awakeFromNib() {
        super.awakeFromNib()

        setupAudioRecorder()
        [lblAudio].forEach { (lbl) in
            lbl?.textColor = UIColor.lightGray
            lbl?.font = themeFont(size: 17, fontname: .regular)
        }
        
        [self.btnAudio].forEach { (btn) in
            btn?.layer.cornerRadius = 4
            btn?.clipsToBounds = true
            btn?.backgroundColor = UIColor.appThemeLightOrangeColor
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setupAudioRecorder() {
        self.btnAudio.translatesAutoresizingMaskIntoConstraints = false
        self.recordView.translatesAutoresizingMaskIntoConstraints = false
        self.vwAudioRecorder.addSubview(self.recordView)
        self.recordView.centerYAnchor.constraint(equalTo: self.vwAudioRecorder.centerYAnchor, constant: 8).isActive = true
        self.recordView.trailingAnchor.constraint(equalTo: self.vwAudioRecorder.trailingAnchor, constant: -30).isActive = true
        self.recordView.leadingAnchor.constraint(equalTo: self.vwAudioRecorder.leadingAnchor, constant: 0).isActive = true
        self.btnAudio.recordView = self.recordView
        self.recordView.delegate = self
    }
    
    func onStart() {
        
        print("onStart")
        [self.btnAudio].forEach { (btn) in
            btn?.layer.cornerRadius = 25
            btn?.clipsToBounds = true
            btn?.backgroundColor = UIColor.red
        }
        
        self.lblAudio.text = ""
        self.setUpRecorder()
        
    }
    
    func onCancel() {
    
        print("onCancel")
        [self.btnAudio].forEach { (btn) in
            btn?.layer.cornerRadius = 4
            btn?.clipsToBounds = true
            btn?.backgroundColor = UIColor.appThemeLightOrangeColor
        }
        self.lblAudio.text = "Voice"
//        self.audioRecorder.deleteRecording()
    }
    
    func onFinished(duration: CGFloat) {
        
        print("onCancel")
        [self.btnAudio].forEach { (btn) in
            btn?.layer.cornerRadius = 4
            btn?.clipsToBounds = true
            btn?.backgroundColor = UIColor.appThemeLightOrangeColor
        }
        
        self.lblAudio.text = self.fileName
        
        self.audioRecorder.stop()
        let url = self.audioRecorder.url
        print("URL: ", url)
        
        DispatchQueue.main.async {
            do {
                let base64 = try Data(contentsOf: url).base64EncodedString()
                self.audioHandlor(self.btnAudio.tag, base64)
            }
            catch {
                print("Error")
            }
        }
    }
}

//MARK: Audio Setup
extension AudioTableCell : AVAudioRecorderDelegate, AVAudioPlayerDelegate {
    
    func getDocumentDirectory() -> URL {
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = paths[0]
        return documentDirectory
    }
    
    func getFileURL() -> URL {
        
        let fileName = self.fileName
        let filePath = getDocumentDirectory().appendingPathComponent(fileName)
        return filePath
    }
        
    func setUpRecorder() {
        
        self.recordingSession = AVAudioSession.sharedInstance()
        do {
            try self.recordingSession?.setCategory(AVAudioSession.Category.playAndRecord)
            try self.recordingSession.setActive(true)
            
            let settings = [AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
            ]
            
            self.removeData()
            
            self.audioRecorder = try AVAudioRecorder(url: getFileURL(), settings: settings)
            self.audioRecorder.delegate = self
            self.audioRecorder.isMeteringEnabled  = true
            self.audioRecorder.prepareToRecord()
            self.audioRecorder.record()
            
        }
        catch let error {
            print("ERROR: ", error.localizedDescription)
        }
    }
    
    ///--- In directory remove last all record file
    func removeData() {
        
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

        do {
            let fileURLs = try FileManager.default.contentsOfDirectory(at: documentsUrl,
                                                                       includingPropertiesForKeys: nil,
                                                                       options: [.skipsHiddenFiles, .skipsSubdirectoryDescendants])
            for fileURL in fileURLs {
                if fileURL.pathExtension == "m4a" {
                    try FileManager.default.removeItem(at: fileURL)
                }
            }
        } catch  { print(error) }
    }
}

