//
//  DropdownTableCell.swift
//  Taal
//
//  Created by Vishal on 17/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class DropdownTableCell: UITableViewCell {

    //MARK: Outlets
    @IBOutlet weak var btnCategory: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        btnCategory.setupThemeButtonUI()
        btnCategory.setTitleColor(UIColor.black, for: .normal)
        btnCategory.backgroundColor = .white
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
