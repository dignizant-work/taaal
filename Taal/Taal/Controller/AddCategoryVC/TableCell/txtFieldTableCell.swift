//
//  txtFieldTableCell.swift
//  Taal
//
//  Created by Vishal on 16/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class txtFieldTableCell: UITableViewCell, UITextFieldDelegate {

    //MARK: Outlets
    @IBOutlet weak var txtValue: SkyFloatingLabelTextField!
    var handlorTxtValue:(_ index: Int, _ value: String,_ isEdit: Bool) -> Void = { _, _, _ in }
    var checkIsEdit = false
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        txtValue.lineHeight = 0
        txtValue.selectedLineHeight = 0
        txtValue.titleFont = themeFont(size: 10, fontname: .regular)
        txtValue.font = themeFont(size: 17, fontname: .regular)
        txtValue.tintColor = .black
        txtValue.titleColor = .appThemeOrangeColor
        txtValue.selectedTitleColor = .appThemeOrangeColor
        txtValue.delegate = self as UITextFieldDelegate
    }
    
    func setUpData(data: CategoryDataModel) {
        
        if data.editValue?.count ?? 0 > 0 {
            checkIsEdit = true
        }
        txtValue.text = data.editValue?.count ?? 0 > 0 ? data.editValue?.first ?? "" : ""
        txtValue.placeholder = data.required == true ? data.label!+"*" : data.label!
        txtValue.title = data.required == true ? data.label!+"*" : data.label!
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        
        if (textField.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            print("Value not available")
        }
        else {
            print("Value available")
            self.handlorTxtValue(textField.tag, textField.text!, self.checkIsEdit)
        }
    }
}
