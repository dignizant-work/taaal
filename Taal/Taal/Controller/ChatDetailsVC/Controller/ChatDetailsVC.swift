//
//  ChatDetailsVC.swift
//  Taal
//
//  Created by Abhay on 26/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper
import SDWebImage
import IQKeyboardManagerSwift
import FirebaseDatabase

class ChatDetailsVC: UIViewController {

    
    //MARK: Variables
    lazy var mainView: ChatDetailsView = { [unowned self] in
        return self.view as! ChatDetailsView
        }()
    
    lazy var mainModelView: ChatDetailsViewModel = {
        return ChatDetailsViewModel(theController: self)
    }()
    
    var ref: DatabaseReference!
    var isFirstTime:Bool = true
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        ref = Database.database().reference()
        updateChatList()
        
        mainView.setUpUI(theDelegate: self)
        
        mainView.registertXIb(theDelegate: self)
        
        conversationID = self.mainModelView.conversatinID
        isChatScreen = true
        self.mainModelView.userChatHistoryApi(conversationID: self.mainModelView.conversatinID) {
            self.mainView.tblMessageChat.reloadData()
            self.scrollToBottom()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.endEditing(true)
        
        setUpNavigationBarWithTitleRightAndBack(strTitle: self.mainModelView.userName, barColor: UIColor.appThemeBlueColor, showTitle: true, isLeftBackButton: true, isRightSkipButton: false)
        mainModelView.setupKeyboard()
        
        //Status Bar Color set
        UINavigationBar.appearance().barStyle = .blackTranslucent
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        conversationID = ""
        isChatScreen = false
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
        self.view.endEditing(true)
        NotificationCenter.default.removeObserver(UIResponder.keyboardWillShowNotification)
        NotificationCenter.default.removeObserver(UIResponder.keyboardWillHideNotification)
    }

}


//MARK: Firebase setup
extension ChatDetailsVC {
    
    func createRoom(){
        let strTime = DateToString(Formatter: "hh:mm a", date: Date())
        let dict = ["time":strTime,
                    "reply":"true",
                    "message":self.mainView.txtVwMessage.text ?? ""] as [String : Any]
       
            
        ref.child("conversations").child(self.mainModelView.conversatinID).setValue(dict)
    }
    
    func updateChatList(){
        
        ref.child("conversations").child("\(mainModelView.conversatinID)").observe(.value) { (snapshot) in
            
            if self.isFirstTime {
                self.isFirstTime = false
                return
            }
            self.setupFirebaseMsg(dictMsg: JSON(snapshot.value!))
        }
    }
    
    func setupFirebaseMsg(dictMsg:JSON) {
        
        var isAlreadyDate = Bool()
        var dataIndex = Int()
        for i in 0..<self.mainModelView.arrayChatDetail.count{
            let dict = self.mainModelView.arrayChatDetail[i]
            let strDate = self.stringTodate(Formatter: "dd-MM-yyyy", strDate: dict.date)
            
            let todayDate = self.DateToString(Formatter: "dd-MM-yyyy", date: Date())
            let date = self.stringTodate(Formatter: "dd-MM-yyyy", strDate: todayDate)
            
            if strDate == date {
                isAlreadyDate = true
                dataIndex = i
                break
            }
        }
        
        let dictMessage = dictMsg
        if isAlreadyDate {
            let dict = self.mainModelView.arrayChatDetail[dataIndex]
            var arr = dict.chat
            let dictChat = Chat()
            dictChat.message = dictMessage["message"].stringValue
            dictChat.time = dictMessage["time"].stringValue
            dictChat.reply = dictMessage["reply"].boolValue
            arr.append(dictChat)
            self.mainModelView.arrayChatDetail[dataIndex].chat = arr
            
        }
        else {
            let dict = ChatDataModel()
            var arr = [Chat]()
            let dictChat = Chat()
            dictChat.message = dictMessage["message"].stringValue
            dictChat.time = dictMessage["time"].stringValue
            dictChat.reply = dictMessage["reply"].boolValue
            arr.append(dictChat)
            dict.chat = arr
            dict.date = self.DateToString(Formatter: "dd-MM-yyyy", date: Date())
            self.mainModelView.arrayChatDetail.append(dict)
        }
        self.mainView.afterMessageSend()
        self.mainView.tblMessageChat.reloadData()
        self.scrollToBottom()
        
    }
    
    func scrollToBottom(){
        if self.mainModelView.arrayChatDetail.count > 0{
            let index = self.mainModelView.arrayChatDetail.count - 1
            if index >= 0 {
                let finalCount = self.mainModelView.arrayChatDetail[index].chat.count - 1
                self.mainView.tblMessageChat.scrollsToTop = false
                self.mainView.tblMessageChat.scrollToRow(at: IndexPath(row: finalCount, section: index), at: .bottom, animated: false)
            }
        }
    }
}

//MARK: Button Action
extension ChatDetailsVC {
    
    @IBAction func btnSendAction(_ sender: UIButton) {
        
        if sender.isUserInteractionEnabled == true {
            self.mainView.activityIndicater.isHidden = false
            sender.isUserInteractionEnabled = false
            print("Send Message")
            var param : [String:Any] = [:]
            param["conversation_id"] = self.mainModelView.conversatinID
            param["message"] = self.mainView.txtVwMessage.text
            self.mainModelView.sendMessageToProviderAPI(param: param) {
                print("Message sent.....")
                self.createRoom()
            }
        }
        else {
            print("Message not sent")
        }
    }
}
