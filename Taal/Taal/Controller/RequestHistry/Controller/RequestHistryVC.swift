//
//  RequestHistryVC.swift
//  Taal
//
//  Created by Vishal on 28/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit

class RequestHistryVC: UIViewController {
    
    //MARK: Variables
    lazy var mainView: RequestHistryView = {[unowned self] in
        return self.view as! RequestHistryView
    }()
    
    lazy var mainModelView: RequestHistryViewModel = {
        return RequestHistryViewModel(theController: self)
    }()
    
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.setUpUI(theController: self)
        self.setUpNavigationBarWithTitleRightAndBack(strTitle: "Requestry_History_key".localized, barColor: .appThemeBlueColor, showTitle: true, isLeftBackButton: true, isRightSkipButton: false)
        self.mainModelView.getCasesHistoryAPI(statusID: self.mainModelView.caseID) {
            self.mainView.tblRequestHistry.reloadData()
        }
        
        setupPullToRefresh()
    }
    
    override func viewDidLayoutSubviews() {
        self.mainView.tblRequestHistry.reloadData()
        self.mainView.tblRequestHistry.layoutIfNeeded()
        self.mainView.tblRequestHistry.layoutSubviews()
    }
    
    @IBAction func btnSuccessAction(_ sender: Any) {
        self.mainView.vwBottomSuccessShow()
        
        if self.mainModelView.caseID != "3" {
            self.mainModelView.caseID = "3"
            self.mainModelView.getCasesHistoryAPI(statusID: self.mainModelView.caseID) {
                self.mainView.tblRequestHistry.reloadData()
            }
        }
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        
        self.mainView.vwBottomCancelShow()
        if self.mainModelView.caseID != "2" {
            self.mainModelView.caseID = "2"
            self.mainModelView.getCasesHistoryAPI(statusID: self.mainModelView.caseID) {
                self.mainView.tblRequestHistry.reloadData()
            }
        }
    }
}

//MARK: SetUpUI
extension RequestHistryVC {
    
    func setupPullToRefresh() {
        
        self.mainModelView.refreshController = UIRefreshControl()
        self.mainModelView.refreshController.backgroundColor = UIColor.clear
        self.mainModelView.refreshController.tintColor = .appThemeBlueColor
        self.mainModelView.refreshController.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        self.mainView.tblRequestHistry.addSubview(self.mainModelView.refreshController)
        
    }
    
    @objc func pullToRefresh() {
        self.mainModelView.getCasesHistoryAPI(statusID: self.mainModelView.caseID) {
            self.mainView.tblRequestHistry.reloadData()
        }
    }
}
