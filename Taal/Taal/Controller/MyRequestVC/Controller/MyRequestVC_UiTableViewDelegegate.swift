//
//  MyRequestVC_UiTableViewDelegegate.swift
//  Taal
//
//  Created by Vishal on 22/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

extension MyRequestVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.mainView.tblMyRequest {
            if self.mainModelView.arrayCaseData.count == 0 {
                setTableView("No_record_found_key".localized, tableView: self.mainView.tblMyRequest)
                return 0
            }
            tableView.backgroundView = nil
            return self.mainModelView.arrayCaseData.count
        }
        
        if self.mainModelView.arrayCaseData.count > tableView.tag {
            let filter = self.mainModelView.arrayCaseData[tableView.tag].fields.filter { (dict) -> Bool in
                let voice = dict.type?.lowercased() != "Voice".lowercased()
                let img = dict.type != "image"
                return voice && img
            }

            return filter.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.mainView.tblMyRequest {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PendingTableCell", for: indexPath) as! PendingTableCell
            
            let data = self.mainModelView.arrayCaseData[indexPath.row]
            cell.setUpData(data: data)
            
            if self.mainModelView.caseID == "1" {
                cell.btnBadge.isHidden = true
            }
            
            //            if data.fields.count > 0 {
            cell.tblDescription.tag = indexPath.row
            cell.tblDescription.delegate = self
            cell.tblDescription.dataSource = self
            cell.tblDescription.reloadData()
            cell.tblDescription.layoutIfNeeded()
            cell.tblDescription.setNeedsLayout()
            cell.tblDescription.layoutSubviews()
            
            //            }
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionTableCell", for: indexPath) as! DescriptionTableCell
        
        let filter = self.mainModelView.arrayCaseData[tableView.tag].fields.filter { (dict) -> Bool in
            let voice = dict.type?.lowercased() != "Voice".lowercased()
            let img = dict.type != "image"
            return voice && img
        }
        
        cell.backgroundColor = .clear
        cell.vwMain.backgroundColor = .clear
        let dict = filter[indexPath.row]
        cell.lblDescriptionTitle.text = "\(dict.label!)"+": "
        cell.lblDescriptionValue.text = dict.value?.first ?? ""
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.mainModelView.caseID == "0" {
            let detailVC = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "RequestDetailVC") as! RequestDetailVC
            detailVC.mainModelView.handlorDeleteCase = {
                self.mainModelView.arrayCaseData.remove(at: indexPath.row)
                self.mainView.tblMyRequest.reloadData()
            }
            detailVC.mainModelView.handlorBaseButtonHide = {
                self.mainModelView.arrayCaseData[indexPath.row].countUnreadQuottion = 0
                let index = IndexPath(row: indexPath.row, section: 0)
                self.mainView.tblMyRequest.reloadRows(at: [index], with: .none)
            }
            detailVC.hidesBottomBarWhenPushed = true
            detailVC.mainModelView.categoryData = self.mainModelView.arrayCaseData[indexPath.row]
            self.navigationController?.pushViewController(detailVC, animated: false)
        }
        else {
            let data = self.mainModelView.arrayCaseData[indexPath.row]
            
            let invoiceVC = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "InvoiceVC") as! InvoiceVC
            invoiceVC.hidesBottomBarWhenPushed = true
            invoiceVC.mainModelView.caseID = "\(data.caseId ?? 0)"
            self.navigationController?.pushViewController(invoiceVC, animated: true)
        }
    }
}

