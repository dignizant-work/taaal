//
//  DescriptionTableCell.swift
//  Taal
//
//  Created by Vishal on 22/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class DescriptionTableCell: UITableViewCell {

    //MARK: Outlets
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var lblDescriptionTitle: UILabel!
    @IBOutlet weak var lblDescriptionValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblDescriptionTitle, lblDescriptionValue].forEach { (lbl) in
            lbl?.font = themeFont(size: 14, fontname: .semiCn)
            lbl?.textColor = UIColor.appThemeBlueColor
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
