//
//  PendingTableCell.swift
//  Taal
//
//  Created by Vishal on 26/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit

class PendingTableCell: UITableViewCell {

    //MARK: Outlets
    @IBOutlet var lblRequest: UILabel!
    @IBOutlet weak var lblRequestValue: UILabel!
    @IBOutlet var lblCategoryTitle: UILabel!
    @IBOutlet var lblCategoryValue: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var tblDescription: UITableView!
    
    @IBOutlet var lblStatusTitle: UILabel!
    @IBOutlet var lblStatusValue: UILabel!
    @IBOutlet var lblQuotationTitle: UILabel!
    @IBOutlet var btnQuotations: UIButton!
    @IBOutlet var btnBadge: CustomButton!
    @IBOutlet weak var tblDescriptionHeightConstraint: NSLayoutConstraint!
    @IBOutlet var vwWriteReview: UIView!
    @IBOutlet var btnWriteReview: CustomButton!
    
    @IBOutlet var vwQuotations: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setUpUI()
        
        [vwQuotations, vwWriteReview].forEach { (vw) in
            vw?.isHidden = true
        }
        
        tblDescription.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        
        self.tblDescription.register(UINib(nibName: "DescriptionTableCell", bundle: nil), forCellReuseIdentifier: "DescriptionTableCell")
        self.tblDescription.tableFooterView = UIView()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
    }
        
    func setUpUI() {
        
        [lblRequest, lblRequestValue, lblCategoryTitle, lblDescription, lblStatusTitle].forEach { (lbl) in
            
            lbl?.font = themeFont(size: 18, fontname: .semiCn)
            lbl?.textColor = UIColor.appThemeBlueColor
        }
        
        [lblCategoryValue, lblStatusValue, lblQuotationTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 14, fontname: .semiCn)
            lbl?.textColor = UIColor.appThemeBlueColor
        }
        
        [lblQuotationTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 14, fontname: .bold)
            lbl?.textColor = UIColor.appThemeOrangeColor
        }
        
        [btnQuotations].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 14, fontname: .bold)
            btn?.setTitleColor(.appThemeOrangeColor, for: .normal)
        }
        
        [btnWriteReview].forEach { (btn) in
            btn?.setupThemeButtonUI()
            btn?.backgroundColor = UIColor.appThemeLightOrangeColor
            btn?.layer.cornerRadius = (btn?.frame.size.height ?? 0)/2
            btn?.setTitle("Write_a_review_key".localized, for: .normal)
            btnWriteReview.setTitleColor(.white, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 15, fontname: .regular)
        }
        
        self.lblRequest.text = "Request_key".localized
        self.btnBadge.isHidden = true
        self.lblCategoryTitle.text = "Categoty_key".localized
        self.lblDescription.text = "Description_key".localized
        self.lblStatusTitle.text = "Status_key".localized
        self.lblQuotationTitle.text = "Quotations_key".localized
    }
    
    func setUpData(data: CategoryDataModel) {
        
        self.lblRequestValue.text = "\(data.caseId ?? 0)"
        self.lblCategoryValue.text = data.category?.name ?? ""
        self.lblStatusValue.text = data.status ?? ""
        
        [vwQuotations, vwWriteReview].forEach { (vw) in
            vw?.isHidden = true
        }
        
        if data.fields.count > 0 {
//            tblDescription.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        }
        
        vwQuotations.isHidden = false
        self.btnBadge.isHidden = true
        if data.countUnreadQuottion != 0 {
            self.btnBadge.isHidden = false
        }
        self.btnBadge.setTitle("\(data.countUnreadQuottion ?? 0)", for: .normal)
        self.btnQuotations.setTitle("\(data.numQuotations ?? 0)", for: .normal)
    }
    
    // Set observer for inner table height
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if(keyPath == "contentSize"){
            if object is UITableView {
                // print("contentSize:= \(tblSubData.contentSize.height)")
                self.contentView.layoutIfNeeded()
                self.contentView.layoutSubviews()
                
                if tblDescription.contentSize.height == 0 {
                    self.tblDescriptionHeightConstraint.constant = 20
                }
                else {
                    self.tblDescriptionHeightConstraint.constant = tblDescription.contentSize.height
                }
            }
        }
    }
    
    deinit {
        self.tblDescription.removeObserver(self, forKeyPath: "contentSize")
    }
}
