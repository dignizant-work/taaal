//
//  ChooseLocationVC.swift
//  Taal
//
//  Created by Vishal on 23/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import SwiftyJSON

class ChooseLocationVC: UIViewController {

    //MARK:- Variables
    lazy var mainView: ChooseLocationView = { [unowned self] in
        return self.view as! ChooseLocationView
        }()
    
    var locationManager: CLLocationManager?
    var dicAddress = NSMutableDictionary()
    
    /*lazy var mainModelView: ChooseLocationViewModel = {
     return ChooseLocationViewModel(theController: self)
     }()*/
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.mainView.setUpUi(theDelegate: self)
        self.navigationController?.navigationBar.isHidden = true
        
        
        gotoHomeTabbar() //remove this when location check
        
        //Uncomment this when continue work
        /*
        self.mainView.vwMap.delegate = self
        appDelegate.setUpLocation()

        let currentMyLocation = appDelegate.locationManager.location?.coordinate

        self.showMap(latitude: currentMyLocation?.latitude ?? 0.0, longitude: currentMyLocation?.longitude ?? 0.0)
 */
 
        setUpNavigationBarWhithTitle(strTitle: "Confirm_location_key".localized)
        
        setUpNavigationDismissButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
    
        userDefault.set(true, forKey: isDefaultAddress)
//        isDefaultAddress = true
        self.mainView.setUpData(theDelegate: self)
    }
    
    func setUpNavigationDismissButton() {
        let btnDIssmiss = UIBarButtonItem(image: UIImage(named: "ic_close_header")?.imageFlippedForRightToLeftLayoutDirection(), style: .plain, target: self, action: #selector(btnDismiss(sender:)))
        btnDIssmiss.tintColor = .white
        navigationItem.rightBarButtonItem = btnDIssmiss
        
    }
    
    @objc func btnDismiss(sender: UIBarButtonItem) {
        
        appDelegate.tabBarController = UserTabBarController()
        let navigation = UINavigationController(rootViewController: appDelegate.tabBarController)
        appDelegate.window?.rootViewController = navigation
        print("Dismiss pressed.....")
        userDefault.removeObject(forKey: defaultAddress)
    }
    
    func showMap(latitude: Double, longitude: Double) {
        
        let cameraCoord = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        self.mainView.vwMap.camera = GMSCameraPosition.camera(withTarget: cameraCoord, zoom: 18.0)
        
    }
    
    //MARK:- Button Action
    @IBAction func btnChooseLocationAction(_ sender: Any) {
        
        if self.dicAddress != nil {
            userDefault.set(self.dicAddress, forKey: defaultAddress)
        }
        let addressData = userDefault.dictionary(forKey: defaultAddress)!
//        print("Address:-",addressData["city"]!)
        appDelegate.tabBarController = UserTabBarController()
        let navigation = UINavigationController(rootViewController: appDelegate.tabBarController)
        appDelegate.window?.rootViewController = navigation
    }
    
}

//MARK: Map delegate
extension ChooseLocationVC : GMSMapViewDelegate {
    func didUpdateLocation(lat: Double!, lon: Double!) {

        print("lat - ",lat)
        print("lon - ",lon)
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {

        print("Lat:", position.target.latitude)
        print("Lat:", position.target.longitude)
        
        currentLocationTitle(lat: position.target.latitude, long: position.target.longitude)
    }
    
    func currentLocationTitle(lat: Double,long: Double)  {
        
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: lat , longitude: long)
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            print("Response GeoLocation :", placemarks ?? "")
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            
            if placeMark != nil {
                // Country
                guard let country = placeMark.addressDictionary?["Country"] as? String else { return }
                print("Country :- \(country)")
                // City
                guard let city = placeMark.addressDictionary?["City"] as? String else { return }
                print("City :- \(city)")
                // State
                guard let state = placeMark.addressDictionary?["State"] as? String else { return }
                print("State :- \(state)")
                // Street
                // ZIP
                guard let zip = placeMark.addressDictionary?["ZIP"] as? String else { return }
                print("ZIP :- \(zip)")
                // Location name
                guard let locationName = placeMark?.addressDictionary?["Name"] as? String else { return }
                print("Location Name :- \(locationName)")
                // Street address
                //                 guard let thoroughfare = placeMark?.addressDictionary?["Thoroughfare"] as? NSString  else { return }
                //                    print("Thoroughfare :- \(thoroughfare)")
                //Street
                var streetNumber = String()
                if let street = placeMark.addressDictionary?["Street"] as? String{
                    print("Street :- \(street)")
                    let str = street
                    
                    streetNumber = str.components(
                        separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
                    
                    self.mainView.txtLocation.text = "\(streetNumber), \(locationName), \(city), \(state)-\(zip), \(country)"
                    
                    print("streetNumber :- \(streetNumber)" as Any)
                }
                
                if streetNumber == "" {
                    self.mainView.txtLocation.text = " \(locationName), \(city), \(state)-\(zip), \(country)"
                }
                
                self.dicAddress["country"] = country
                self.dicAddress["city"] = city
                self.dicAddress["state"] = state
                self.dicAddress["zip"] = zip
                self.dicAddress["locationName"] = locationName
                self.dicAddress["streetNumber"] = streetNumber
            }
        })
    }
}



