//
//  SignUpView.swift
//  Taal
//
//  Created by Vishal on 22/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit

class SignUpView: UIView {
    
    //MARK: Outlets
    @IBOutlet var vwMain: UIView!
    @IBOutlet var imgHeader: UIImageView!
    @IBOutlet var lblPhoneNo: UILabel!
    @IBOutlet var btnPhoneSelection: UIButton!
    @IBOutlet var txtUsername: CustomTextField!
    @IBOutlet var txtPhoneNo: CustomTextField!
    @IBOutlet var txtEmail: CustomTextField!
    @IBOutlet var btnSignUp: UIButton!
    @IBOutlet var lblWantWork: UILabel!
    @IBOutlet var btnProvider: UIButton!
    @IBOutlet var lblSigningAgree: UILabel!
    @IBOutlet var btnPrivacyPolicy: UIButton!
    
    
    //MARK: SetUp
    
    func setUpUI(theDelegate:SignUpVC) {
        
        txtUsername.placeholder = "Username_key".localized
        txtEmail.placeholder = "Email_key".localized
        txtPhoneNo.placeholder = "Phone_number_key".localized
        btnSignUp.setTitle("Signup_key".localized, for: .normal)
        lblWantWork.text = "Want_to_work_as_a_key".localized
        lblSigningAgree.text = "By_signing_up_agree_with_key".localized
        btnProvider.setTitle("Provider_key".localized, for: .normal)
        btnPrivacyPolicy.setTitle("Privacy_policy_key".localized, for: .normal)
        
        [lblPhoneNo, lblWantWork, lblSigningAgree].forEach { (lbl) in
            lbl?.textColor = UIColor.lightGray
            lbl?.font = themeFont(size: 11, fontname: .regular)
        }
        
        [txtUsername, txtEmail, txtPhoneNo].forEach { (txt) in
            txt?.font = themeFont(size: 16, fontname: .regular)
            txt?.textColor = UIColor.appThemeBlueColor
        }
        
        [btnProvider, btnPrivacyPolicy].forEach { (btn) in
            btn?.setTitleColor(.lightGray, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 13, fontname: .regular)
        }
        
        btnProvider.setTitleColor(UIColor.black, for: .normal)
    }
}
