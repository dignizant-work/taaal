//
//  SignUpVC.swift
//  Taal
//
//  Created by Vishal on 22/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit
import SwiftyJSON
import FirebaseAuth

class SignUpVC: UIViewController {

    //MARK:- Variables
    lazy var mainView: SignUpView = { [unowned self] in
        return self.view as! SignUpView
    }()
    
    lazy var mainViewModel: SignUpViewModel = {
        return SignUpViewModel(theController: self)
    }()
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        mainView.setUpUI(theDelegate: self)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpNavigationBarWithTitleRightAndBack(strTitle: "", barColor: UIColor.appThemeBlueColor, showTitle: false, isLeftBackButton: true, isRightSkipButton: false)
//        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
}

//MARK: Firebase OTP setup
extension SignUpVC {
    
    func setupMobileNumberOTP()  {
        
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        
        let phonenumber = "\(self.mainView.lblPhoneNo.text!)"+"\(self.mainView.txtPhoneNo.text!)"
        print("phonenumber:-\(phonenumber)")
        
        PhoneAuthProvider.provider().verifyPhoneNumber(phonenumber, uiDelegate: nil) {(verificationid, error) in
            
            if error == nil {
                print("verificationid:-\(verificationid!)")
                guard let verify = verificationid else {return}
                userDefault.set(verify, forKey: "verifyID")
                
                let mobileVC = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "VerifyPhoneVC") as! VerifyPhoneVC
                mobileVC.mainModelView.verifyId = verify
                mobileVC.mainModelView.selectController = .register
                mobileVC.mainModelView.signupDataDict = self.mainViewModel.signupDataDict
                self.navigationController?.pushViewController(mobileVC, animated: false)
            }
            else {
                makeToast(strMessage: error?.localizedDescription ?? "")
                print("unable to get verification:", error?.localizedDescription as Any)
            }
        }
    }
}


//MARK: Button Action
extension SignUpVC {
    
    @IBAction func btnPhoneSelection(_ sender: Any) {
        
        let obj = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "CountryCodeVC") as! CountryCodeVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.mainView.delegate = self
        self.present(obj, animated: false, completion: nil)
    }
    
    @IBAction func btnSignUpAction(_ sender: Any) {
        
        if (self.mainView.txtUsername.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            makeToast(strMessage: "Please enter username")
        }
        else if (self.mainView.txtEmail.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            makeToast(strMessage: "Please enter email")
        }
        else if (self.mainView.txtPhoneNo.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            makeToast(strMessage: "Please enter phone number.")
        }
        else if self.mainView.txtPhoneNo.text?.isNumeric == false {
            makeToast(strMessage: "Please enter  valid number")
        }
        else {
            let param : [String:String] = ["mobile": self.mainView.txtPhoneNo.text! ,
                                           "country_code": self.mainViewModel.countryCode,
                                           "email":self.mainView.txtEmail.text!]

            self.mainViewModel.signupDataDict = JSON(param)
            self.mainViewModel.signupDataDict["fullname"].stringValue = self.mainView.txtUsername.text!
            self.mainViewModel.mobileEmailIsExist(param: param) {
                self.setupMobileNumberOTP()
            }
            
//            let mobileVC = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "VerifyPhoneVC") as! VerifyPhoneVC
//            self.navigationController?.pushViewController(mobileVC, animated: true)
        }
    }
    
    @IBAction func btnProviderAction(_ sender: Any) {
        
    }
    
    @IBAction func btnPrivacyPolicyAction(_ sender: Any) {
        
        let TermsPolicyVC = AppStoryboard.Profile.instance.instantiateViewController(withIdentifier: "TermsPolicyVC") as! TermsPolicyVC
        TermsPolicyVC.mainViewModel.isTerms = false
        TermsPolicyVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(TermsPolicyVC, animated: false)
    }
}

//MARK: Country code delegate
extension SignUpVC : CountryCodeDelegate {
    
    func CountryCodeDidFinish(data: JSON) {
        self.mainView.lblPhoneNo.text = data["dial_code"].string ?? ""
        
        print("data:\(data)")
        self.mainViewModel.countryCode = data["dial_code"].stringValue
        
        //        self.mainView.countryDialCode = data["dial_code"].string ?? ""
        //        self.mainView.countryName = data["name"].string ?? ""
        //        self.mainView.countryCode = data["code"].string ?? ""
        //        self.mainView.imgCountryCode.image = UIImage(named : self.mainView.countryCode)
        //        self.mainView.lblCountryCode.text = mainView.countryDialCode
    }
    
}
