//
//  VerifyPhoneVC.swift
//  Taal
//
//  Created by Vishal on 23/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAuth
import FirebaseMessaging


class VerifyPhoneVC: UIViewController {

    //MARK:- Variables
    lazy var mainView: VerifyPhoneView = { [unowned self] in
        return self.view as! VerifyPhoneView
        }()
    
    lazy var mainModelView: VerifyPhoneViewModel = {
        return VerifyPhoneViewModel(theController: self)
    }()
    
    //MARK:- Variable
    var enteredOTP : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.mainView.setUpUI(theDelegate: self)
        self.navigationController?.navigationBar.isHidden = true
        self.view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        UIApplication.shared.statusBarView?.backgroundColor = .white
    }
    
    override func viewDidDisappear(_ animated: Bool) {
//        UIApplication.shared.statusBarView?.backgroundColor = ColorNavigationStatusBar()
    }

    
    func verifyOtp() {

        let verificationID = userDefault.value(forKey: "verifyID")
        
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID as! String,
            verificationCode: enteredOTP)
        
        Auth.auth().signIn(with: credential) { (user, error) in
            if error == nil {
                if user != nil {
                    user?.user.getIDToken(completion: { (data, error) in
                        if error == nil {
//                            if self.theControllerModel.selectController != .update {
//
//                            }
                            let token : String = data ?? ""
                            userDefault.set(token, forKey: "idToken")
                            idToken = userDefault.value(forKey: "idToken") as! String
                            print("IdToken:", token)
                            print("")
                            
                            if self.mainModelView.selectController == .login {
                                self.loginUserAPI()
                            }
                            else if self.mainModelView.selectController == .register {
                                print("Register")
                                self.registerUserApi()
                            }
                            else if self.mainModelView.selectController == .update {
                                self.updateMobileNumberApi()
                            }
                            print("Verify Successfully")
                        }
                    })
                                        
                    let firebaseAuth = Auth.auth()
                    do {
                        try firebaseAuth.signOut()
                    } catch let signOutError as NSError {
                        print ("Error signing out: %@", signOutError)
                    }
                }
            }
            else {
                makeToast(strMessage: error?.localizedDescription ?? "")
                print("Error:-\(error?.localizedDescription as Any)")
            }
        }
        
//        Auth.auth().signIn(with: credential) { (user, error) in
//            if let error = error {
//                print(error.localizedDescription)
//                APPDEL.window?.makeToast("OTP entered is incorrect")
//                complition(false)
//                return
//            }
//            complition(true)
//        }
    }
}

//MARK:- Button Action
extension VerifyPhoneVC {
    
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnContinueAction(_ sender: Any) {
        
        if enteredOTP.count < 6 {
            makeToast(strMessage: "Please enter OTP code")
        }
        else {
            
            self.verifyOtp()
//            if userDefault.value(forKey: ADDRESS) == nil {
//                let signUp = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "ChooseLocationVC") as! ChooseLocationVC
//                self.navigationController?.pushViewController(signUp, animated: true)
//            }
//            else {
//                appDelegate.tabBarController = UserTabBarController()
//                let navigation = UINavigationController(rootViewController: appDelegate.tabBarController)
//                appDelegate.window?.rootViewController = navigation
//            }
        }
    }
    
    @IBAction func btnResendNowAction(_ sender: Any) {
        
    }
    
}


//MARK: API SetUp
extension VerifyPhoneVC {
    
    func loginUserAPI() {
        
        let param = ["idToken" : idToken,
                     "mobile" : self.mainModelView.signupDataDict["mobile"].stringValue,
                     "country_code" : self.mainModelView.signupDataDict["country_code"].stringValue,
                     "player_id" : Messaging.messaging().fcmToken ?? "",
                     "device_type" : deviceType] as [String : Any]
        
        mainModelView.loginUserAPI(param: param) {
                        
            if userDefault.object(forKey: isDefaultAddress) == nil {
                let objc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "ChooseLocationVC") as! ChooseLocationVC
                self.navigationController?.pushViewController(objc, animated: false)
            }
            else {
                if userDefault.bool(forKey: isDefaultAddress) == false {
                    let objc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "ChooseLocationVC") as! ChooseLocationVC
                    self.navigationController?.pushViewController(objc, animated: false)
                }
                else {
                    appDelegate.tabBarController = UserTabBarController()
                    let navigation = UINavigationController(rootViewController: appDelegate.tabBarController)
                    appDelegate.window?.rootViewController = navigation
                }
            }
        }
    }
    
    func registerUserApi() {
        
        let param = ["idToken":idToken,
                     "fullname":self.mainModelView.signupDataDict["fullname"].stringValue,
                     "email":self.mainModelView.signupDataDict["email"].stringValue,
                     "mobile":self.mainModelView.signupDataDict["mobile"].stringValue,
                     "country_code":self.mainModelView.signupDataDict["country_code"].stringValue,
                     "player_id":Messaging.messaging().fcmToken ?? "",
                     "device_type":deviceType] as [String : Any]
        
        print("Param:-", param)
        mainModelView.registerUserAPI(param: param, completionHandlor: {
            print("Register:")
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: LoginVC.self) {
                    self.navigationController?.popToViewController(controller, animated: false)
                    break
                }
            }
        })
    }
    
    func updateMobileNumberApi() {
        let param = ["idToken":idToken,
                     "mobile":self.mainModelView.signupDataDict["mobile"].stringValue,
                     "country_code":mainModelView.signupDataDict["country_code"].stringValue] as [String : Any]
        print("Param:-", param)
        
        self.mainModelView.updateMobileNumberAPI(param: param) {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: ProfileVC.self) {
                    self.navigationController?.popToViewController(controller, animated: false)
                    break
                }
            }
        }
    }
}
