//
//  ProfileView.swift
//  Taal
//
//  Created by Abhay on 25/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit

class ProfileView: UIView {
    
    //MARK: Outlets
    
    @IBOutlet weak var imgProfile:UIImageView!
    @IBOutlet weak var txtName:UITextField!
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var btnSelectImg: UIButton!
    @IBOutlet weak var btnUpdate: CustomButton!
    @IBOutlet weak var tblProfile: UITableView!
    
    func setupUI(theDelegate: ProfileVC) {
        imgProfile.layer.cornerRadius = imgProfile.bounds.width/2
        imgProfile.clipsToBounds = true
        
        txtName.font = themeFont(size: 18, fontname: .semiCn)
        txtName.isUserInteractionEnabled = false
        
        txtEmail.font = themeFont(size: 14, fontname: .light)
        txtEmail.isUserInteractionEnabled = false
        btnSelectImg.isUserInteractionEnabled = true
        tblProfile.register(UINib(nibName: "ProfileCell", bundle: nil), forCellReuseIdentifier: "ProfileCell")
        
//        theDelegate.mainModelView.arrayHeader = ["My_Chats_key".localized, "Addresses_key".localized,"Request_history_key".localized,"Change_mobile_key".localized,"Settings_key".localized,"Terms_Of_use_key".localized,"Privacy_policy_key".localized,"App_feedback".localized,"Logout_key".localized]
//        theDelegate.mainModelView.arrayImgHeader = ["ic_my_chats", "ic_address","ic_request_history","ic_change_mobile","ic_settings","ic_terms_of_use","ic_privacy_policy","ic_feedback","ic_logout"]
        
        theDelegate.mainModelView.arrayHeader = ["My_Chats_key".localized, "Addresses_key".localized,"Request_history_key".localized,"Change_mobile_key".localized,"Settings_key".localized,"Privacy_policy_key".localized,"App_feedback".localized,"Logout_key".localized]
        theDelegate.mainModelView.arrayImgHeader = ["ic_my_chats", "ic_address","ic_request_history","ic_change_mobile","ic_settings","ic_privacy_policy","ic_feedback","ic_logout"]
        
        tblProfile.dataSource = theDelegate
        tblProfile.delegate = theDelegate
        btnUpdate.setTitle("EDIT_key".localized, for: .normal)
    }
}

