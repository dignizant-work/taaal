//
//  ProfileVC_UiTableViewDelegates.swift
//  Taal
//
//  Created by Abhay on 26/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import FirebaseMessaging

extension ProfileVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.mainModelView.arrayHeader.count == 0
        {
            let lbl = UILabel()
            lbl.text = "List not found yet"
            lbl.numberOfLines = 5
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.appThemeBlueColor
            lbl.center = tableView.center
            lbl.font = themeFont(size: 16.0, fontname: .light)
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return self.mainModelView.arrayHeader.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell") as! ProfileCell
        if indexPath.row == 0{
            DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
                cell.backView.roundCorners([.topLeft,.topRight], radius: 6)
            }
            
        }
        if indexPath.row == self.mainModelView.arrayHeader.count-1{
            DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
                cell.backView.roundCorners([.bottomLeft,.bottomRight], radius: 6)
            }
        }
        if self.mainModelView.arrayHeader.count > 0
        {
            //cell.setData(data: arrayHeader[indexPath.row])
            cell.lblTitle.text = "\(self.mainModelView.arrayHeader[indexPath.row])"
            cell.imgProfile.image = UIImage(named: "\(self.mainModelView.arrayImgHeader[indexPath.row])")?.imageFlippedForRightToLeftLayoutDirection()
        }
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50//UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let title = self.mainModelView.arrayHeader[indexPath.row]
        
        if title == "My_Chats_key".localized {
            
            let myChatVC = AppStoryboard.Profile.instance.instantiateViewController(withIdentifier: "MyChatVC") as! MyChatVC
            
            myChatVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(myChatVC, animated: true)
        }
        else if title == "Addresses_key".localized {
            
            let addressesVC = AppStoryboard.Profile.instance.instantiateViewController(withIdentifier: "AddressesVC") as! AddressesVC
            
            addressesVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(addressesVC, animated: true)
        }
        else if title == "Request_history_key".localized {
            
            let requestHistoryVC = AppStoryboard.Profile.instance.instantiateViewController(withIdentifier: "RequestHistryVC") as! RequestHistryVC
            
            requestHistoryVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(requestHistoryVC, animated: true)
        }
        else if title == "Change_mobile_key".localized {
            
            let ChangePasswordVC = AppStoryboard.Profile.instance.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
            ChangePasswordVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(ChangePasswordVC, animated: true)
        }
        else if title == "Settings_key".localized {
            let SettingVC = AppStoryboard.Profile.instance.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
            SettingVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(SettingVC, animated: true)
        }
        else if title == "Terms_Of_use_key".localized {
            
            let TermsPolicyVC = AppStoryboard.Profile.instance.instantiateViewController(withIdentifier: "TermsPolicyVC") as! TermsPolicyVC
            TermsPolicyVC.mainViewModel.isTerms = true
            TermsPolicyVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(TermsPolicyVC, animated: true)
        }
        else if title == "Privacy_policy_key".localized {
            
            let TermsPolicyVC = AppStoryboard.Profile.instance.instantiateViewController(withIdentifier: "TermsPolicyVC") as! TermsPolicyVC
            TermsPolicyVC.mainViewModel.isTerms = false
            TermsPolicyVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(TermsPolicyVC, animated: true)
        }
        else if title == "App_feedback".localized {
            let addFeedbackVC = AppStoryboard.Profile.instance.instantiateViewController(withIdentifier: "AddFeedbackVC") as! AddFeedbackVC
            addFeedbackVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(addFeedbackVC, animated: true)
        }
        else if title == "Logout_key".localized  {
            
            DispatchQueue.main.async {
                self.LogoutButtonAction()
            }
        }
        
    }
    @objc func LogoutButtonAction()
    {
        let alertController = UIAlertController(title: "Taal_key".localized, message: "Are_you_sure_want_to_logout_?_key".localized, preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title:"Yes_key".localized, style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            
            let param = ["token":Messaging.messaging().fcmToken ?? ""]
            self.logoutApi(param: param)
            
//            let LoginVC = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//            let navigation = UINavigationController(rootViewController: LoginVC)
//            appDelegate.window?.rootViewController = navigation
        }
        
        let cancelAction = UIAlertAction(title:"No_key".localized, style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
