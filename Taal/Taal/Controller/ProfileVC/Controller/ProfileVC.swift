                       //
//  ProfileVC.swift
//  Taal
//
//  Created by Abhay on 25/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit
import TOCropViewController

class ProfileVC: UIViewController {
    
    //MARK:- Variables
    lazy var mainView: ProfileView = { [unowned self] in
        return self.view as! ProfileView
        }()
    
    lazy var mainModelView: ProfileViewModel = {
        return ProfileViewModel(theController: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        mainView.setupUI(theDelegate: self)
        mainView.txtName.text = "User name"
        mainView.txtEmail.text = "user email"
        
        self.mainModelView.getUserProfileAPI {
            print("Success")
            self.mainView.txtName.text = getUserData()?.user?.fullname
            self.mainView.txtEmail.text = getUserData()?.user?.email
        }
        self.isNotificationChatVC()
        
        let btnshare = UIBarButtonItem(image: UIImage(named: "ic_share")?.imageFlippedForRightToLeftLayoutDirection(), style: .plain, target: self, action: #selector(btnShareAction(_:)))
        btnshare.tintColor = UIColor.white
        navigationItem.rightBarButtonItem = btnshare
    }
    
    override func viewWillAppear(_ animated: Bool) {
        isRequestListVC = true
        setUpNavigationBarWithTitleRightAndBack(strTitle: "Profile_key".localized, barColor: UIColor.appThemeBlueColor, showTitle: true, isLeftBackButton: false, isRightSkipButton: false)
    }
    
    func checkValidation() {
        
        if ((mainView.txtName.text?.trimmingCharacters(in: .whitespaces).isEmpty)!) {
            makeToast(strMessage: "Please_enter_username_key".localized)
        }
        else if ((mainView.txtEmail.text?.trimmingCharacters(in: .whitespaces).isEmpty)!) {
            makeToast(strMessage: "Please_enter_email_key".localized)
        }
        else if self.mainView.txtName.text == getUserData()?.user?.fullname && self.mainView.txtEmail.text == getUserData()?.user?.email {
            mainView.txtName.isUserInteractionEnabled = false
            mainView.txtEmail.isUserInteractionEnabled = false
            self.view.endEditing(true)
            mainView.btnSelectImg.isUserInteractionEnabled = false
            mainView.btnUpdate.setTitle("EDIT_key".localized, for: .normal)
        }
        else {
            
            var param : [String:Any] = [:]
            param["fullname"] = self.mainView.txtName.text ?? ""
            param["email"] = self.mainView.txtEmail.text ?? ""
            //                param["mobile"] = getUserData()?.user?.mobile
            param["image"] = ""
            //                self.mainModelView.imageData
            print("param:",param)
            
            self.mainModelView.updateUserProfileAPI(param: param) {
                self.mainModelView.getUserProfileAPI {
                    print("Success")
                    self.mainView.txtName.text = getUserData()?.user?.fullname
                    self.mainView.txtEmail.text = getUserData()?.user?.email
                }
            }
        }
    }
    
    func isNotificationChatVC() {
        
        if isNotificationChat {
            
            let myChatVC = AppStoryboard.Profile.instance.instantiateViewController(withIdentifier: "MyChatVC") as! MyChatVC
            
            myChatVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(myChatVC, animated: false)
        }
    }
    
    @objc func btnShareAction(_ sender: UIBarButtonItem) {
        print("Share")
        //Set the default sharing message.
        let message = ""
        //Set the link to share.
        if let link = NSURL(string: "itms-apps://itunes.apple.com/app/") {
            let objectsToShare = [message,link] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
            self.present(activityVC, animated: true, completion: nil)
        }
    }
}

//MARK: Button Action
extension ProfileVC {
    
    @IBAction func btnUpdateAction(_ sender: CustomButton) {
        if mainView.btnUpdate.titleLabel?.text == "EDIT_key".localized {
            mainView.txtName.isUserInteractionEnabled = true
            mainView.txtEmail.isUserInteractionEnabled = true
            mainView.txtName.becomeFirstResponder()
            mainView.btnSelectImg.isUserInteractionEnabled = true
            mainView.btnUpdate.setTitle("UPDATE_key".localized, for: .normal)
        }
        else {
            mainView.txtName.isUserInteractionEnabled = false
            mainView.txtEmail.isUserInteractionEnabled = false
            self.view.endEditing(true)
            mainView.btnSelectImg.isUserInteractionEnabled = false
            mainView.btnUpdate.setTitle("EDIT_key".localized, for: .normal)
            checkValidation()
        }
    }
    
    @IBAction func btnSelectImgAction(_ sender: UIButton) {
        
        mainModelView.customImagePicker.typeOfPicker = .onlyPhoto
        mainModelView.customImagePicker.showImagePicker(fromViewController: self, navigationColor: UIColor.appThemeBlueColor, imagePicked: { (response) in
            let themeImage = response[UIImagePickerController.InfoKey.originalImage] as! UIImage
            self.openCropVC(Image: themeImage)
            
        }, imageCanceled: {
        }, imageRemoved: nil)
    }
    
}

//MARK:- TOCropViewcontroller Delegate
extension ProfileVC: TOCropViewControllerDelegate {
    
    func openCropVC(Image:UIImage) {
        self.view.endEditing(true)
        //        let cropVC = TOCropViewController(image: Image)
        let cropVC = TOCropViewController(croppingStyle: TOCropViewCroppingStyle.circular, image: Image)
        cropVC.customAspectRatio = CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.width)
        cropVC.delegate = self
        //        cropVC.navigationController?.isNavigationBarHidden = true
        cropVC.aspectRatioPreset = .presetCustom
        cropVC.rotateButtonsHidden = true
        cropVC.aspectRatioPickerButtonHidden = true
        cropVC.rotateClockwiseButtonHidden = true
        cropVC.resetAspectRatioEnabled = false
        cropVC.toolbar.backgroundColor = UIColor.white
        cropVC.toolbarPosition = .top
        cropVC.toolbar.cancelTextButton.setTitleColor(UIColor.white, for: .normal)
        cropVC.toolbar.doneTextButton.setTitleColor(UIColor.white, for: .normal)
        self.present(cropVC, animated: false, completion: nil)
    }
    
    /*func cropViewController(_ cropViewController: TOCropViewController, didCropTo image: UIImage, with cropRect: CGRect, angle: Int) {
        self.mainView.imgProfile.image = image
        cropViewController.dismiss(animated: false, completion: nil)
    }*/
    func cropViewController(_ cropViewController: TOCropViewController, didCropToCircularImage image: UIImage, with cropRect: CGRect, angle: Int) {
        self.mainView.imgProfile.image = image
        cropViewController.dismiss(animated: false, completion: nil)
    }
   
    func cropViewController(_ cropViewController: TOCropViewController, didFinishCancelled cancelled: Bool) {
        
        dismiss(animated: true, completion: nil)
    }
}
