//
//  PendingView.swift
//  Taal
//
//  Created by Vishal on 26/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit

class PendingView: UIView {
    
    //MARK: Outlets
    @IBOutlet var tblPending: UITableView!
    
    //MARK: SetUpUI
    func setUpUI(theDelegate: PendingVC) {
        
        registerXib()
    }
    
    func registerXib() {
        
        tblPending.register(UINib(nibName: "PendingTableCell", bundle: nil), forCellReuseIdentifier: "PendingTableCell")
        tblPending.tableFooterView = UIView()
    }
    
}
