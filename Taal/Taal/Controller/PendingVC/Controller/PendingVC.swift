//
//  PendingVC.swift
//  Taal
//
//  Created by Vishal on 26/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit

class PendingVC: UIViewController {

    //MARK: Variables
    lazy var mainView: PendingView = { [unowned self] in
        return self.view as! PendingView
    }()
    
    lazy var mainModelView: PendingModelView = {
        return PendingModelView(theController: self)
    }()
    
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.setUpUI(theDelegate: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
}
