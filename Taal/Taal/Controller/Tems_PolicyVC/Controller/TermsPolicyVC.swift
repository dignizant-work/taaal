//
//  TermsPolicyVC.swift
//  Taal
//
//  Created by Abhay on 30/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit
import WebKit

class TermsPolicyVC: UIViewController {

    //MARK: Variables
    lazy var mainView: TermsPolicyView = { [unowned self] in
        return self.view as! TermsPolicyView
        }()
    
    lazy var mainViewModel: TermsPolicyViewModel = {
        return TermsPolicyViewModel(theController: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.mainView.setUpUI(theController: self)
        if mainViewModel.isTerms {
            self.mainViewModel.isTitile = "Terms_Of_use_key".localized
            
            //For T&C param pass : "setting_type" = "terms-and-condition", For privacy policy param: "setting_type" = "privacy-and-policy" >
            let param = ["setting_type" : "terms-and-condition"]
            self.mainViewModel.getPagesDataURl(param: param) { data in
                let htmlString = data["terms"].stringValue
                self.mainViewModel.webView.loadHTMLString(htmlString, baseURL: nil)
            }
        }
        else {
            self.mainViewModel.isTitile = "Privacy_policy_key".localized
            let param = ["setting_type" : "privacy-and-policy"]
            self.mainViewModel.getPagesDataURl(param: param) { data in
                let htmlString = data["privacy"].stringValue
                self.mainViewModel.webView.loadHTMLString(htmlString, baseURL: nil)
            }
        }
        self.setUpNavigationBarWithTitleRightAndBack(strTitle: self.mainViewModel.isTitile, barColor: .appThemeBlueColor, showTitle: true, isLeftBackButton: true, isRightSkipButton: false)
    }
    override func viewWillAppear(_ animated: Bool) {
//        let url = URL(string: "https://stripe.com/au/connect-account/legal")
//        let webRequest : NSURLRequest = NSURLRequest(url: url!)
//        self.mainViewModel.webView.load(webRequest as URLRequest)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension TermsPolicyVC: WKNavigationDelegate{
    func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
        print(#function)
       //stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        print(#function)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print(#function)
       //stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print(#function)
         //showLoader()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print(#function)
        //stopAnimating()
    }
    
}
