//
//  CustomTabBarController.swift
//  Taal
//
//  Created by Haresh on 04/06/19.
//  Copyright © 2019 Haresh. All rights reserved.
//

import UIKit

class UserTabBarController: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()
        DispatchQueue.global().sync {
            setUpTabBar()
            dispatchGroup.leave()
        }
        
    }
    
    
    func setUpTabBar() {
        
        //TODO:  Setup
        
        tabBarController?.tabBar.frame.size.height = 75
        
        /*-------*/
        let nav1 = UINavigationController()
        let first = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        
        let firstTab = UITabBarItem(title: "Home_key".localized, image: UIImage(named: "ic_home_button_unselect"), selectedImage: UIImage(named: "ic_home_button_select")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        firstTab.imageInsets = UIEdgeInsets(top: 3, left: 0, bottom: 0, right: 0)
        firstTab.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 1)
        first.tabBarItem = firstTab
        nav1.viewControllers = [first]
        nav1.setNavigationBarHidden(true, animated: true)
        
       /* /*-------*/
        let nav2 = UINavigationController()
        let second = AppStoryboard.User.instance.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        let secondTab = UITabBarItem(title: getCommonString(key: "Cart_key"), image: UIImage(named: "ic_cart_image"), selectedImage: UIImage(named: "ic_cart_select_button")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        second.tabBarItem = secondTab
        nav2.viewControllers = [second]
        nav2.setNavigationBarHidden(true, animated: true)*/
        
        /*-------*/
        let nav3 = UINavigationController()
        let third = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "MyRequestVC") as! MyRequestVC
        let thirdTab = UITabBarItem(title: "MyRequest_key".localized, image: UIImage(named: "ic_my_requests_unselect"), selectedImage: UIImage(named: "ic_my_requests_select")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        thirdTab.imageInsets = UIEdgeInsets(top: 3, left: 0, bottom: 0, right: 0)
        thirdTab.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 1)
        third.tabBarItem = thirdTab
        third.mainModelView.selectParentController = .fromTab
        nav3.viewControllers = [third]
        nav3.setNavigationBarHidden(true, animated: true)

        /*-------*/
        let nav4 = UINavigationController()
        let fourth = AppStoryboard.Profile.instance.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        let fourthTab = UITabBarItem(title: "Profile_key".localized.uppercased(), image: UIImage(named: "ic_profile_unselect"), selectedImage: UIImage(named: "ic_profile_select")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        fourthTab.imageInsets = UIEdgeInsets(top: 3, left: 0, bottom: 0, right: 0)
        fourthTab.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 1)
        fourth.tabBarItem = fourthTab
        nav4.viewControllers = [fourth]
        nav4.setNavigationBarHidden(true, animated: true)
        

            let unselectedItem = [NSAttributedString.Key.foregroundColor: UIColor.black,
                                  NSAttributedString.Key.font :  themeFont(size: 10, fontname: .regular)]
            let selectedItem = [NSAttributedString.Key.foregroundColor: UIColor.black,
                                NSAttributedString.Key.font :  themeFont(size: 10, fontname: .regular)]
            
             UITabBarItem.appearance().setTitleTextAttributes(unselectedItem, for: .normal)
             UITabBarItem.appearance().setTitleTextAttributes(selectedItem, for: .selected)
        UITabBar.appearance().tintColor = UIColor.appThemeBlueColor
        //( red: CGFloat(255/255.0), green: CGFloat(99/255.0), blue: CGFloat(95/255.0), alpha: CGFloat(1.0) )
        
        self.viewControllers = [nav1,nav3,nav4]
        
        //TOP line hide
        self.tabBar.layer.borderWidth = 0
        self.tabBar.clipsToBounds = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         appDelegate.tabBarController.delegate = self
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        let navigationController = viewController as? UINavigationController
        navigationController?.popToRootViewController(animated: true)
        
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        guard
            //let tab = tabBarController.viewControllers?.index(of: viewController), contains(tab as! UIFocusEnvironment)
            let tab = tabBarController.viewControllers?.index(of: viewController), [0,2].contains(tab)
            else {
                if let vc = tabBarController.viewControllers?[tabBarController.selectedIndex] as? UINavigationController {
                    vc.dismiss(animated: true, completion: nil)
                    vc.popToRootViewController(animated: false)
                }
                return true
        }
        return true
    }
    
}

extension UIImage {
    
//    func imageWithImage( newWidth: CGFloat, height: CGFloat) -> UIImage {
//
//        let newSize = CGSize(width: newWidth-1, height: height)
//        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
//        self.draw(in: CGRect(origin: CGPoint(x: 0, y: 10), size: CGSize(width: newSize.width, height: newSize.height)))
//        let newImage : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
//        UIGraphicsEndImageContext()
//        return newImage
//    }
    
    class func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
        let rect: CGRect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }

}
