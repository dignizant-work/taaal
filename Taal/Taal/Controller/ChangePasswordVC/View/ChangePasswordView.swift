//
//  ChangePasswordView.swift
//  Taal
//
//  Created by Abhay on 29/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit

class ChangePasswordView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var lblCountryCode: UITextField!
    @IBOutlet weak var txtMobileNumber: UITextField!
    @IBOutlet weak var btnUpdate: CustomButton!
    @IBOutlet weak var lblChangeMobileNumber: UILabel!
    
    
    func setUpUI(theController: ChangePasswordVC) {
        lblCountryCode.font = themeFont(size: 16, fontname: .regular)
        txtMobileNumber.font = themeFont(size: 16, fontname: .regular)
        btnUpdate.titleLabel?.font = themeFont(size: 18, fontname: .regular)
        lblChangeMobileNumber.font = themeFont(size: 10, fontname: .regular)
        txtMobileNumber.text = getUserData()?.user?.mobile
        lblCountryCode.text = getUserData()?.user?.countryCode
    }
    
}

