//
//  LoginVC.swift
//  Taal
//
//  Created by Vishal on 22/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit
import SwiftyJSON
import NMLocalizedPhoneCountryView
import FirebaseAuth
import Firebase

class LoginVC: UIViewController {

    //MARK: Variables
    lazy var mainView : LoginView = {[unowned self] in
        return self.view as! LoginView
    }()
    
    lazy var mainModelView: LoginViewModel = {
        return LoginViewModel(theController: self)
    }()
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isHidden = true
        self.mainView.setUpUI()
        addDoneButtonOnKeyboard(textfield: mainView.txtPhoneNo)
        if getUserData()?.accessToken == nil || getUserData()?.accessToken == nil {
            
        }
        else {
            appDelegate.tabBarController = UserTabBarController()
            appDelegate.tabBarController.selectedIndex = 0
            self.navigationController?.pushViewController(appDelegate.tabBarController, animated: false)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
}


extension LoginVC {
    
    func setupMobileNumberOTP()  {
        
        let phonenumber = "\(self.mainModelView.countryCode) "+"\(self.mainView.txtPhoneNo.text!)"
//            self.mainView.txtPhoneNo.text else { return }
        
        PhoneAuthProvider.provider().verifyPhoneNumber(phonenumber, uiDelegate: nil) {(verificationid, error) in
            
            if error == nil {
                
                print("verificationid:-\(verificationid ?? "")")
                guard let verify = verificationid else {return}
                userDefault.setValue(verify, forKey: "verifyID")
                
                let mobileVC = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "VerifyPhoneVC") as! VerifyPhoneVC
                mobileVC.mainModelView.verifyId = verify
                mobileVC.mainModelView.selectController = .login
                mobileVC.mainModelView.signupDataDict = self.mainModelView.loginDataDict
                self.navigationController?.pushViewController(mobileVC, animated: false)
            }
            else {
                print("unable to get verification:", error?.localizedDescription ?? "")
            }
        }
    }
}


//MARK: Button action
extension LoginVC {
    
    @IBAction func btnLoginAction(_ sender: Any) {
        
        if (self.mainView.txtPhoneNo.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            makeToast(strMessage: "Please enter phone number.")
        }
        else if self.mainView.txtPhoneNo.text?.isNumeric == false {
            makeToast(strMessage: "Please enter  valid number")
        }
        else {
            
            let dict = ["mobile" : self.mainView.txtPhoneNo.text ?? "",
                        "country_code" : self.mainModelView.countryCode] as [String : Any]
            print("Param:- \(dict)")
            self.mainModelView.loginDataDict = JSON(dict)
            self.mainModelView.checkIsMobileEmailExist(param: dict) {
                self.setupMobileNumberOTP()
            }
            
//            let mobileVC = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "VerifyPhoneVC") as! VerifyPhoneVC
//            self.navigationController?.pushViewController(mobileVC, animated: true)
        }
        
//        appDelegate.tabBarController = UserTabBarController()
//        let navigation = UINavigationController(rootViewController: appDelegate.tabBarController)
//        appDelegate.window?.rootViewController = navigation
    }
    
    @IBAction func btnTermsConditionAction(_ sender: Any) {
        let TermsPolicyVC = AppStoryboard.Profile.instance.instantiateViewController(withIdentifier: "TermsPolicyVC") as! TermsPolicyVC
        TermsPolicyVC.mainViewModel.isTerms = true
        TermsPolicyVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(TermsPolicyVC, animated: true)
    }
    
    @IBAction func btnPhoneSelectionAction(_ sender: Any) {
        
        let obj = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "CountryCodeVC") as! CountryCodeVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.mainView.delegate = self
        self.present(obj, animated: false, completion: nil)
    }
    
    @IBAction func btnSignUpHereAction(_ sender: Any) {
        let signUp = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
            self.navigationController?.pushViewController(signUp, animated: false)
        }
    }
}


extension LoginVC : CountryCodeDelegate {
    
    func CountryCodeDidFinish(data: JSON) {
        //        self.mainView.countryDialCode = data["dial_code"].string ?? ""
        //        self.mainView.countryName = data["name"].string ?? ""
        //        self.mainView.countryCode = data["code"].string ?? ""
        //
        
        self.mainView.lblPhoneNo.text = data["dial_code"].string ?? ""
        
        //        self.mainView.imgCountryCode.image = UIImage(named : self.mainView.countryCode)
        //        self.mainView.lblCountryCode.text = mainView.countryDialCode
    }
    
}
