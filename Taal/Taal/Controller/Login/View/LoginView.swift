//
//  LoginView.swift
//  Taal
//
//  Created by Vishal on 22/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import NMLocalizedPhoneCountryView

class LoginView: UIView {
    
    //MARK: Outlets
    @IBOutlet var vwMain: UIView!
    @IBOutlet var imgHeader: UIImageView!
    @IBOutlet var lblPhoneNo: UILabel!
    @IBOutlet var btnPhoneSelection: UIButton!
    @IBOutlet var txtPhoneNo: CustomTextField!
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var lblCountinueAgree: UILabel!
    @IBOutlet var btnTermsCondition: UIButton!
    @IBOutlet var lblDontHaveAccount: UILabel!
    @IBOutlet var btnSignUpHere: UIButton!
    
    
    //MARK: SetUp
    
    func setUpUI() {
        
        btnLogin.setTitle("Login_key".localized, for: .normal)
        btnTermsCondition.setTitle("Terms_Condition_key".localized, for: .normal)
        btnSignUpHere.setTitle("Signup_here_key".localized, for: .normal)
        
        lblCountinueAgree.text = "By_countinuing_agree_key".localized
        lblDontHaveAccount.text = "Don't_have_account_key".localized
        
        txtPhoneNo.placeholder = "Phone_number_key".localized
        
        btnLogin.titleLabel?.textColor = .lightGray
        
        [lblPhoneNo, lblCountinueAgree, lblDontHaveAccount].forEach { (lbl) in
            lbl?.textColor = UIColor.lightGray
            lbl?.font = themeFont(size: 11, fontname: .regular)
        }
        
        [btnSignUpHere, btnTermsCondition].forEach { (btn) in
            btn?.setTitleColor(.lightGray, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 13, fontname: .regular)
        }
        
    }
    
}
