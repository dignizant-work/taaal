//
//  RequestDetailViewModel.swift
//  Taal
//
//  Created by Vishal on 27/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class RequestDetailViewModel {
    
    fileprivate weak var theController:RequestDetailVC!
    var categoryData = CategoryDataModel(JSON: JSON().dictionaryValue)
    var caseDetailData : CategoryDataModel?
    var handlorDeleteCase:()->Void = {}
    var handlorBaseButtonHide:()->Void = {}
    
    
    //MARK: Initializer
    init(theController: RequestDetailVC) {
        self.theController = theController
    }
    
}

//MARK: API Setup
extension RequestDetailViewModel {
    
    func getCaseDetailAPI(caseID: String, completionHandlor:@escaping()->Void) {
        
        let url = getcaseDetailsURL+caseID
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        
        print("URL: ", url)
        print("Header: ", header)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        self.theController.showLoader()
        
        WebServices().MakeGetAPI(name: url, params: [:], header: header) { (response, error, statusCode) in
            
            self.theController.hideLoader()
            if statusCode == success {
                if let dict = response as? NSDictionary {
                    let jsonDict = JSON(dict).dictionaryObject!
                    print("JSONDict: ", jsonDict)
                    self.caseDetailData = CategoryDataModel(JSON: jsonDict)
                    print("")
                }
                completionHandlor()
            }
            else if statusCode == notFound {
                completionHandlor()
            }
            else if error != nil {
                print("Error: ", error?.localized ?? "")
            }
            else {
                print("StatusCode: ", statusCode ?? 0)
            }
        }
    }
    
    
    func deleteCaseAPI(caseID: String, completionHandlor:@escaping()->Void) {
        
        let url = deleteCaseURL+caseID
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        
        print("URL: ", url)
        print("Header: ", header)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        self.theController.showLoader()
        WebServices().MakeDeleteAPI(name: url, params: [:], header: header) { (response, error, statusCode) in
            
            self.theController.hideLoader()
            if statusCode == success {
                if let dict = response {
                    let jsonDict = JSON(dict).dictionaryValue
                    makeToast(strMessage: jsonDict["message"]?.stringValue ?? "")
                }
                completionHandlor()
            }
            else if statusCode == notFound {
                completionHandlor()
            }
            else if error != nil {
                print("Error: ", error?.localized ?? "")
            }
            else {
                print("StatusCode: ", statusCode ?? 0)
            }
        }
    }
    
    func caseCancelledAPI(caseID: String, completionHandlor:@escaping()->Void) {
        
        let url = caseCancelledURL+caseID
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        
        print("URL: ", url)
        print("Header: ", header)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        self.theController.showLoader()
        WebServices().MakePatchAPI(name: url, params: [:], header: header) { (response, error, statusCode) in
            
            self.theController.hideLoader()
            if statusCode == success {
                if let dict = response {
                    let jsonDict = JSON(dict).dictionaryValue
                    makeToast(strMessage: jsonDict["message"]?.stringValue ?? "")
                }
                completionHandlor()
            }
            else if statusCode == notFound {
                completionHandlor()
            }
            else if error != nil {
                print("Error: ", error?.localized ?? "")
            }
            else {
                print("StatusCode: ", statusCode ?? 0)
            }
        }
    }
}
