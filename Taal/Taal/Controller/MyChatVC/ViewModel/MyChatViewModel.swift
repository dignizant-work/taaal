//
//  MyChatViewModel.swift
//  Taal
//
//  Created by Abhay on 26/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class MyChatViewModel {
    
    //MARK:- Variables
    fileprivate weak var theController:MyChatVC!
    
    //MARK: Initialized
    init(theController:MyChatVC) {
        self.theController = theController
    }
    
    //MARK: Variables
    var arrayHeader : [String] = []
    var currentIndex = 0
    var isOwner = false
    var arrayChatList : [ChatDataModel] = []
    var refreshController : UIRefreshControl!
    
    
    func userChatListAPI(completionHandlor:@escaping()->Void) {
        let url = userChatListURL
        print("URL:- ", url)
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        print("URL: ", url)
        print("Header: ", header)
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        WebServices().MakeGetAPI(name: url, params: [:], header: header) { (response, error, statusCode) in
            self.theController.hideLoader()
            self.arrayChatList = []
            if statusCode == success {
                if let array = response as? NSArray {
                    let jsonArray = JSON(array).arrayObject!
                    print("ChatArray: ", JSON(array).arrayValue)
                    for data in jsonArray {
                        let model = ChatDataModel(JSON: JSON(data).dictionaryObject!)
                        self.arrayChatList.append(model!)
                    }
                }
                completionHandlor()
            }
            else if statusCode == notFound {
                completionHandlor()
            }
            else if error != nil {
                print("Error: ", error?.localized ?? "")
            }
            else {
                print("StatusCode: ", statusCode ?? 0)
            }
        }
    }
}
