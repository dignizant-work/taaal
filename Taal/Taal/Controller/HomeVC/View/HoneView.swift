//
//  HoneView.swift
//  Taal
//
//  Created by Abhay on 25/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit

class HoneView: UIView {
    
    //MARK: Outlets
    
    
    @IBOutlet weak var txtSearch: CustomTextField!
    @IBOutlet weak var tblHome: UITableView!
    @IBOutlet weak var vwTblTop: UIView!
    
    func setupUI(theDelegate: HomeVC) {
        txtSearch.placeholder = "What_are_you_looking_for_key".localized
        [txtSearch].forEach { (txt) in
            txt?.textColor = UIColor.black
            txt?.font = themeFont(size: 14, fontname: .regular)
        }
        txtSearch.rightImage = UIImage(named: "ic_search")?.imageFlippedForRightToLeftLayoutDirection()
        
        tblHome.register(UINib(nibName: "HomeMainCell", bundle: nil), forCellReuseIdentifier: "HomeMainCell")
        
        theDelegate.mainModelView.arrayHeader = ["Driver", "Painter","Plumber","Mechenical"]
        tblHome.dataSource = theDelegate
        tblHome.delegate = theDelegate
        
        vwTblTop.clipsToBounds = true
        vwTblTop.layer.cornerRadius = 8
        //vwTblTop.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top right corner, Top left corner respectively

    }
}

