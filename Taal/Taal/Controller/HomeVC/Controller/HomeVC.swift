//
//  HomeVC.swift
//  Taal
//
//  Created by Abhay on 25/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit
import FirebaseMessaging

class HomeVC: UIViewController {

    //MARK:- Variables
    lazy var mainView: HoneView = { [unowned self] in
        return self.view as! HoneView
        }()
    
    lazy var mainModelView: HomeViewModel = {
        return HomeViewModel(theController: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if getUserData()?.accessToken != nil {
            self.checkDeviceLanguageDataApi()
            if isCompletedNotification {
                isCompletedNotification = false
                let invoiceVC = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "InvoiceVC") as! InvoiceVC
                invoiceVC.hidesBottomBarWhenPushed = true
                invoiceVC.mainModelView.caseID = dictNotificationData["record_id"].stringValue
                self.navigationController?.pushViewController(invoiceVC, animated: false)
            }
            
            self.mainView.txtSearch.addTarget(self, action: #selector(textFieldEditingChanged(_:)), for: .editingChanged)
            self.mainModelView.categoryListApi(isShowLoader: true) {
//                print("Data")
                self.mainView.tblHome.reloadData()
            }
            mainView.setupUI(theDelegate: self)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        isRequestListVC = true
        self.mainModelView.categoryListApi(isShowLoader: false) {
            print("Data")
            self.mainView.tblHome.reloadData()
        }
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("")
    }

    func checkDeviceLanguageDataApi() {
        
        let param = ["player_id":Messaging.messaging().fcmToken ?? ""]
        self.mainModelView.changeLanguageAPI(param: param) {
            print("Success")
        }
    }
}

extension HomeVC : UITextFieldDelegate {
    
    @objc func textFieldEditingChanged(_ textField: UITextField) {
        let searchText  = textField.text ?? ""
        if searchText.count > 2 {
            let arrSearch = self.mainModelView.arrayCategoryData.filter{ ($0.name?.lowercased().contains(textField.text!.lowercased()))!}
            self.mainModelView.arrayCategoryData = arrSearch
            self.mainView.tblHome.reloadData()
        }
        else {
            self.mainModelView.arrayCategoryData = self.mainModelView.arrayAllData
            self.mainView.tblHome.reloadData()
        }
    }
    
}
