//
//  HomeVioewModel.swift
//  Taal
//
//  Created by Abhay on 25/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class HomeViewModel {
    
    //MARK:- Variables
    fileprivate weak var theController:HomeVC!
    
    //MARK: Initialized
    init(theController:HomeVC) {
        self.theController = theController
    }
    
    //MARK: Variables
    var arrayHeader : [String] = []
    var currentIndex = 0
    var isOwner = false
    var arrayCategoryData : [CategoryDataModel] = []
    var arrayAllData : [CategoryDataModel] = []
    
}

//MARK: Api setup
extension HomeViewModel {
    
    func categoryListApi(isShowLoader: Bool, completionHandlor:@escaping()->Void) {
        
        let url = categoryListURL
        let param = NSDictionary() as! [String:Any]
        var header:[String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        if isShowLoader {
            self.theController.showLoader()
        }
        WebServices().MakeGetAPI(name: url, params: param, header: header) { (response, error, statusCode) in
            
            if isShowLoader {
                self.theController.hideLoader()
            }
            if statusCode == success {
                
                self.arrayAllData = []
                self.arrayCategoryData = []
                if let array = response as? NSArray {
                    let data = JSON(array).arrayValue
                    
                    for value in data {
                        let categoryData = CategoryDataModel(JSON: value.dictionaryObject!)
                        self.arrayCategoryData.append(categoryData!)
                        self.arrayAllData.append(categoryData!)
                    }
//                    print("JSON: ", data)
                }
                else if let dict = response as? NSDictionary {
                    let data = JSON(dict)
                    print("JSON: ", data)
                }
                completionHandlor()
            }
            else if statusCode == notFound {
                completionHandlor()
            }
            else if error != nil {
//                makeToast(strMessage: error?.localized ?? "")
                completionHandlor()
            }
        }
    }
    
    func changeLanguageAPI(param:[String:Any], completionHandlor:@escaping()->Void) {
        
        let url = changeLangURL
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Content-Type":"application/json"]
        
        print("URL: ", url)
        print("Param: ", param)
        print("Header: ", header)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        WebServices().MakePostAPI(name: url, params: param, header: header) { (response, error, statusCode) in
            
            if statusCode == success {
                if let data = response {
                    let dictJosn = JSON(data)
                    print("Data: ", dictJosn)
                }
                completionHandlor()
            }
            else if statusCode == notFound {
                
            }
            else if error != nil {
                print("Error: ", error?.localized ?? "")
            }
            else {
                print("StatusCode: ", statusCode ?? 0)
            }
        }
    }
}

