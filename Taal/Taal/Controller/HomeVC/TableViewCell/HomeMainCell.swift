//
//  HomeMainCell.swift
//  Taal
//
//  Created by Abhay on 25/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit

class HomeMainCell: UITableViewCell {

    @IBOutlet weak var btnTitle: CustomButton!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        imgIcon.layer.cornerRadius = imgIcon.frame.height/2
        
        btnTitle.titleLabel?.font = themeFont(size: 26.0, fontname: .bold)
        lblTitle.font = themeFont(size: 26.0, fontname: .bold)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    /*func setData(data:){
        
    }*/
    
}
